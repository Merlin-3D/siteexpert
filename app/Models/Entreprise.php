<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
    //
    protected $fillable = [
        'prestataire_id',
        'nom',
        'tache',
        'date_publication',
        'contact',
        'temoignage'
    ];

    public function prestataire()
    {
        return $this->belongsTo('App\Models\Prestataire', 'prestataire_id', 'id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}
