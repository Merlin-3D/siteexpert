<?php

namespace App\Models;

use Carbon\Carbon;
use COM;
use Illuminate\Database\Eloquent\Model;

class Prestataire extends Model
{
    //
    protected $fillable = [
        'user_id',
        'nom',
        'prenom',
        'email',
        'age',
        'sexe',
        'ville',
        'quartier',
        'description',
        'telephone',
        'whatsapp',
        'file',
        'click',
        'click_contact',
        'banniere'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function categorie()
    {
        return $this
            ->belongsToMany(Categories::class, 'categories_prestataire')
            ->withPivot('id','prix_min','prix_max');;
    }

    public function entreprise()
    {
        return $this->hasMany(Entreprise::class);
    }

    public function commentaire()
    {
        return $this->hasMany(Commentaire::class);
    }
}
