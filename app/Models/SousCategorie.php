<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SousCategorie extends Model
{
    //
    protected $fillable = [
        'categorie_id',
        'titre_sous_categorie'
    ];

    public function categorie()
    {
        return $this->belongsTo('App\Models\Categories', 'categorie_id', 'id');
    }

}
