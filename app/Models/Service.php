<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'categories_id',
        'cat_prest_id',
        'titreService',
        'niveau',

    ];

    public function categorie()
    {
        return $this->belongsTo('App\Models\Categories', 'categorie_id', 'id');
    }

}
