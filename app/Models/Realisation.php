<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Realisation extends Model
{
    //

    protected $fillable = [
        'categories_id',
        'cat_prest_id',
        'url',
        'description'
    ];

    public function categorie()
    {
        return $this->belongsTo('App\Models\Categories', 'categorie_id', 'id');
    }

}
