<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //

    protected $fillable = [
        'titre',
    ];

    public function prestataire()
    {
        return $this
            ->belongsToMany(Prestataire::class, 'categories_prestataire')
            ->withPivot('id','prix_min','prix_max');
    }

    public function souscategorie()
    {
        return $this->hasMany(SousCategorie::class);
    }

    public function service()
    {
        return $this->hasMany(Service::class);
    }

    public function realisation()
    {
        return $this->hasMany(Realisation::class);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
}
