<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Entreprise;
use App\Models\Prestataire;
use App\Models\Realisation;
use App\Models\Service;
use App\Models\SousCategorie;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PrestataireController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:PRESTATAIRE');
    }

    public function index($id)
    {
        try {
            $prestataire = Prestataire::where('user_id', $id)->firstOrFail();

            $click_prestataire = $prestataire->click;
            $competences = DB::table('categories_prestataire')->where('prestataire_id', '=', $prestataire->id)->count();
            $entreprises = Entreprise::where('prestataire_id', $prestataire->id)->count();


            return view('prestataire.home', compact('click_prestataire', 'competences', 'entreprises'));
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function profilPrestataire($id)
    {
        try {
            $users = User::find($id);
            $prestataire = Prestataire::where('user_id', $id)->firstOrFail();
            return view('prestataire.profil', compact('prestataire', 'users'));
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
        }
    }

    public function modifierProfil(Request $request)
    {
        try {
            if ($request->file) {
                $request->validate([
                    'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);

                if ($request->file('file')) {
                    $image = $request->file('file');
                    $input['file'] = time() . '.' . $image->getClientOriginalExtension();
                    $imgFile = \Intervention\Image\Facades\Image::make($image->getRealPath());
                    $imgFile->save(public_path('avatars/' . $input['file']));
                }
                Prestataire::where('user_id', $request->user_id)->update(array(
                    'nom' => $request->nom,
                    'prenom' => $request->prenom,
                    'email' => $request->email,
                    'age' => $request->age,
                    'sexe' => $request->sexe,
                    'ville' => $request->ville,
                    'quartier' => $request->quartier,
                    'description' => $request->description,
                    'telephone' => $request->telephone,
                    'whatsapp' => $request->whatsapp,
                    // 'taux_horaire' => $request->taux_horaire,
                    'file' => $input['file']
                ));
                dd($input['file']);
            } else {
                $prestat =  Prestataire::where('user_id', $request->user_id)->update(array(
                    'nom' => $request->nom,
                    'prenom' => $request->prenom,
                    'email' => $request->email,
                    'age' => $request->age,
                    'sexe' => $request->sexe,
                    'ville' => $request->ville,
                    'quartier' => $request->quartier,
                    'description' => $request->description,
                    'telephone' => $request->telephone,
                    'whatsapp' => $request->whatsapp,
                    // 'taux_horaire' => $request->taux_horaire,

                ));
                dd("ffd");
            }

            return response()->json(['message' => 'Votre profil a été mise a jour avec succé'], 200);
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }

    public function competence($id)
    {
        try {

            $categories = Categories::with('prestataire')->with('souscategorie')->with('service')->get();
            $prestataire = Prestataire::where('user_id', $id)->firstOrFail()->id;

            return view('prestataire.competence', compact('categories', 'prestataire'));
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }

    public function chercherSousCategorie($id)
    {
        try {
            $sousCategories = SousCategorie::where('categories_id', $id)->get();
            return response()->json(['array' =>  $sousCategories]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }

    public function ajouterCompetence(Request $request)
    {
        try {
            $prestataire = Prestataire::where('user_id', $request->user_id)->firstOrFail();
            $query = DB::table('categories_prestataire')->where('categories_id', '=', $request->categorie_id)->where('prestataire_id', '=', $prestataire->id)->get();

            if (count($query) > 0) {
                return response()->json(['warrning' =>  "Vous avez déja cette compétence !"]);
            } else {

                $id_competence =  DB::table('categories_prestataire')->insertGetId(

                    [
                        'categories_id' => $request->categorie_id,
                        'prestataire_id' => $prestataire->id,
                        'prix_min' => $request->prix_min,
                        'prix_max' => $request->prix_max,

                    ],

                );
                for ($i = 0; $i < count($request->sous_cat); $i++) {
                    for ($j = 0; $j < count($request->competences); $j++) {
                        if ($i == $j) {
                            Service::create([
                                'categories_id' => $request->categorie_id,
                                'cat_prest_id' => $id_competence,
                                'titreService' => $request->sous_cat[$i],
                                'niveau' => $request->competences[$j]
                            ]);
                        }
                    }
                }
            }
            //$categories = Categories::with('prestataire')->with('souscategorie')->get();

            return response()->json([
                'success' =>  "Compétence Définie!",
                'array' => $prestataire->id
            ]);
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function supprimerCompetence($categories_id, $prestataire_id, $id)
    {
        try {
            DB::table('categories_prestataire')->where('categories_id', '=', $categories_id)->where('prestataire_id', '=', $prestataire_id)->delete();
            Service::where('cat_prest_id', $id)->delete();
            return response()->json([
                'success' => "compétence supprimer avec succés",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function entreprise($id)
    {
        try {
            $prestataire = Prestataire::where('user_id', $id)->firstOrFail();
            $entreprises = Entreprise::where('prestataire_id', $prestataire->id)->get();
            return view('prestataire.entreprise', compact('entreprises'));
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function ajouterEntreprise(Request $request)
    {

        try {
            $prestataire = Prestataire::where('user_id', $request->user_id)->firstOrFail();

            $entreprise = new Entreprise();
            $entreprise->prestataire_id = $prestataire->id;
            $entreprise->nom = $request->nom;
            $entreprise->tache = $request->tache;
            $entreprise->date_publication =  $request->date_publication;
            $entreprise->contact =  $request->contact;
            $entreprise->temoignage =  $request->temoignage;
            $entreprise->save();

            return response()->json([
                'success' =>  "Entreprise ajouter",
                'array' => $entreprise->getRawOriginal()
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function supprimerEntreprise($id)
    {
        try {
            Entreprise::find($id)->delete();
            return response()->json([
                'success' => "Entreprise supprimer avec succés",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function realisation($compt_id, $titre_categ, $categorie_id)
    {
        $realisations = Realisation::where('cat_prest_id', $compt_id)->get();
        return view('prestataire.realisation', compact('compt_id', 'titre_categ', 'categorie_id', 'realisations'));
    }

    public function ajouterRealisation(Request $request)
    {
        try {
            $request->validate([
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            if ($request->file('file')) {
                $image = $request->file('file');
                $input['file'] = time() . '.' . $image->getClientOriginalExtension();
                $imgFile = \Intervention\Image\Facades\Image::make($image->getRealPath());
                $imgFile->save(public_path('realisations/' . $input['file']));
            }

            $realisation = new Realisation();
            $realisation->categories_id = $request->categorie_id;
            $realisation->cat_prest_id = $request->compt_id;
            $realisation->description = $request->description;
            $realisation->url = $input['file'];
            $realisation->save();

            return response()->json([
                'success' => "Réalisation ajouter",
                'array' => $realisation
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function supprimerRealisation($id)
    {
        try {
            Realisation::where('id', $id)->delete();
            return response()->json([
                'success' => "Réalisation supprimer avec succés",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function uploadBanner(Request $request)
    {
        try {
            $request->validate([
                'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            if ($request->file('file')) {
                $image = $request->file('file');
                $input['file'] = time() . '.' . $image->getClientOriginalExtension();
                $imgFile = \Intervention\Image\Facades\Image::make($image->getRealPath());
                $imgFile->save(public_path('bannieres/' . $input['file']));
            }
            $prestataire = Prestataire::where('user_id', $request->user_id)->firstOrFail();
            Prestataire::where('id', $prestataire->id)->update(array(
                'banniere' => $input['file'],
            ));
            return response()->json([
                'message' => "Bannière mise a jour",
            ]);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->validator->getMessageBag()], 422);
        }
    }
}
