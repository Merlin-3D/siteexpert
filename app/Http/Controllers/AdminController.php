<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Client;
use App\Models\Commentaire;
use App\Models\Prestataire;
use App\Models\SousCategorie;
use App\Models\User;
use Dotenv\Exception\ValidationException;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ADMIN');
    }

    public function index()
    {
        $prestataire = Prestataire::all()->count();
        $client = Client::all()->count();
        $categorie = Categories::all()->count();
        return view('admin.home', compact('prestataire','client','categorie'));
    }

    public function compteAdmin(){
        try {
            $users = User::where('type', 'admin')->get();
            return view('admin.administration', compact('users'));
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }

    }

    public function ajouterAdmin(Request $request){
        try {
            request()->validate(
                [
                    'name' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|min:8',
                ],
                [
                    'name.required' => 'Le champ nom est requis!',
                    'email.required' => 'Le champ email est requis!',
                    'email.unique' => 'l\'adresse email est déja utilisé!',
                    'password.min' => 'Le mot de passe doit contenir au moins 8 charactères',
                    'password.required' => 'Le champ mot de passe est requis!',

                ]
            );

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'type' => 'admin'
            ]);

            DB::table('role_user')->insert(
                [
                    [
                        'role_id' => "1",
                        'user_id' => $user->id
                    ],
                ]
            );
            return response()->json([
                'success' => "Admin ajouter avec succès",
                'users'=> $user
            ]);

        } catch (Exception $e) {
            return response()->json(['errors' => $e->validator->getMessageBag()], 422);
        }

    }

    public function supprimerAdmin($id){
        try {
            User::where('id', $id)->delete();
            return response()->json([
                'success' => "Admin supprimer avec succès",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function acceuilCategorie()
    {
        $categories = Categories::with('souscategorie')->get();
        return view('admin.categorie', compact('categories'));
    }

    public function ajouterCategorie(Request $request){
        try {

            request()->validate(
                [
                    'titre' => 'required',
                ],
                [
                    'titre.required' => 'Le champ titre est requis!',
                ]
            );
            $categorie =  Categories::create([
                'titre' => $request->titre,
            ]);
            if($request->sous_cat){
                for ($i = 0; $i < count($request->sous_cat); $i++) {
                $sousCategorie = new SousCategorie();
                $sousCategorie->categories_id = $categorie->id;
                $sousCategorie->titre_sous_categorie =$request->sous_cat[$i];
                $sousCategorie->save();
            }
            }

            return response()->json([
                'message' => 'Catégorie ajouter avec succès',
            ]);
        } catch (Exception $e) {

            return response()->json(['errors' => $e->validator->getMessageBag()], 422);
        }
    }

    public function supprimerCategorie($id){
        try {
            Categories::where('id', $id)->delete();
            return response()->json([
                'success' => "Catégorie supprimer avec succès",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function prestataire()
    {
        $prestataires = Prestataire::with('user')->get();
        return view('admin.prestataire', compact('prestataires'));
    }

    public function ajouterPrestataire(Request $request)
    {
        try {

            request()->validate(
                [
                    'nom' => 'required',
                    'prenom' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|min:8',
                ],
                [
                    'nom.required' => 'Le champ nom est requis!',
                    'prenom.required' => 'Le champ Prénom est requis!',
                    'email.unique' => 'l\'adresse email est déja utilisé!',
                    'password.min' => 'Le mot de passe doit contenir au moins 8 charactères',

                ]
            );

            $input = $request->all();

            $user =  User::create([
                'name' => $input['nom'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),

            ]);

            $prestataire = new Prestataire();
            $prestataire->user_id = $user->id;
            $prestataire->nom = $request->nom;
            $prestataire->prenom = $request->prenom;
            $prestataire->email = $request->email;
            $prestataire->save();

            DB::table('role_user')->insert(
                [
                    [
                        'role_id' => "2",
                        'user_id' => $user->id
                    ],

                ]
            );

            return response()->json([
                'message' => 'Prestataire créer avec succès',
                'prestataire' => $prestataire
            ]);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->validator->getMessageBag()], 422);
        }
    }

    public function supprimerPrestataire($id, $user_id)
    {
        try {
            Prestataire::where('id', $id)->delete();
            User::where('id', $user_id)->delete();
            return response()->json([
                'success' => "Prestataire supprimer avec succès",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function commentaire()
    {
        $commentaires = Commentaire::with('client')->with('prestataire')->get();
        return view('admin.commentaire', compact('commentaires'));
    }

    public function supprimerCommentaire($id)
    {
        try {
            Commentaire::where('id', $id)->delete();
            return response()->json([
                'success' => "Commentaire supprimer",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function desactiverPrestataire($user_id, $value)
    {
        try {
            if ($value == 0) {
                User::where('id', $user_id)->update(array('status' => 0));
            } else {
                User::where('id', $user_id)->update(array('status' => 1));
            }
            return response()->json([
                'id' => $user_id,
                'value' => $value
            ]);
        } catch (Exception $e) {
            return response()->json(['errors' => $e->validator->getMessageBag()], 422);
        }
    }
}
