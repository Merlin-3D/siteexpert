<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Client;
use App\Models\Commentaire;
use App\Models\ContactUs;
use App\Models\Entreprise;
use App\Models\Prestataire;
use App\Models\Realisation;
use App\Models\Service;
use App\Models\SousCategorie;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
    public function acceuil()
    {
        $vedettes = Prestataire::orderBy('click', 'desc')->limit(5)->get();
        $categories = Categories::with('souscategorie')->get();
        return view('client.home', compact('vedettes', 'categories'));
    }

    public function contact()
    {
        $categories = Categories::with('souscategorie')->get();
        return view('client.contact', compact('categories'));
    }

    public function prestataire($id)
    {
        try {
            $prestataires = Prestataire::where('id', $id)->with('entreprise')->get();
            $commentaires = Commentaire::where('prestataire_id', $id)->with('client')->get();
            $count_entreprise = Entreprise::where('prestataire_id', $id)->count();
            $categories = Categories::with('prestataire')->with('souscategorie')->with('service')->with('realisation')->get();
            $realisations = Realisation::where('cat_prest_id', 107)->get();
            Prestataire::where('id', $id)->update(array(
                'click' => $prestataires[0]->click + 1,
            ));
            // foreach ($categories as $key => $value) {
            //   foreach ($value->prestataire as $key => $value2) {
            //       if ($value2->id == 1) {
            //           dd($value2);
            //       }
            //   }
            // }
            // $commentaires = Prestataire::where('id', $id)->with('commentaire')->get();
            return view('client.prestataire', compact(
                'prestataires',
                'commentaires',
                'count_entreprise',
                'categories'
            ));
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
        }
    }

    public function ville($nom)
    {
        $categories = Categories::with('souscategorie')->get();
        $prestataires = Prestataire::where('ville', $nom)->paginate(8);
        // $prestataires = DB::table('categories_prestataire')
        //     ->join('prestataires', 'categories_prestataire.prestataire_id', '=', 'prestataires.id')
        //     ->get();
        return view('client.ville', compact('categories', 'prestataires'));
    }

    public function categorie($id)
    {

        try {
            $categories = Categories::with('souscategorie')->get();
            if ($id == 0) {
                $prestataires = Prestataire::paginate(8);

                return view('client.categories', compact('categories', 'prestataires'));
            }

            $prestataires = DB::table('categories_prestataire')
                ->join('prestataires', 'categories_prestataire.prestataire_id', '=', 'prestataires.id')
                ->where('categories_id', $id)
                ->paginate(8);

            return view('client.categories', compact('categories', 'prestataires',));
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
        }
    }
    public function devenirFreelancer()
    {
        $categories = Categories::with('souscategorie')->get();
        return view('client.freelancer', compact('categories'));
    }

    public function creerClient(Request $request)
    {
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],

            ]);

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            DB::table('role_user')->insert(
                [
                    [
                        'role_id' => "3",
                        'user_id' => $user->id
                    ],

                ]
            );
            return view('client.home');
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }

    public function ajouterCommentaire(Request $request)
    {
        try {

            request()->validate(
                [
                    'commentaire' => 'required',
                ],
                [
                    'commentaire.required' => 'Le champ commentaire est requis!',
                ]
            );
            $client = Client::where('user_id', $request->client_id)->firstOrFail();
            $commentaire =  Commentaire::create([
                'client_id' => $client->id,
                'commentaire' => $request->commentaire,
                'prestataire_id' => $request->prestataire_id
            ]);
            return response()->json([
                'array' => $commentaire
            ]);
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
            return response()->json(['errors' => $e->validator->getMessageBag()], 422);
        }
    }
    public function rechercheAccueil(Request $request)
    {
        try {
            $service = Service::where('categories_id', $request->id_categorie)->where('titreService', $request->service)->get();

            $categories = Categories::with('souscategorie')->get();

            if (count($service) < 0) {
                $ville = "Oupps...!!! Aucun resultat  pour cette recherche";
                return view('client.home', compact('categories', 'ville'));
            }

            foreach ($service as $key => $value) {

                $prestataires = DB::table('prestataires')
                    ->join('categories_prestataire', 'prestataires.id', '=', 'categories_prestataire.prestataire_id')
                    ->where('categories_id', $value->categories_id)
                    ->get();
            }
            if (empty($prestataires)) {
                $ville = "Oupps...!!! Aucun resultat  pour cette recherche";
                return view('client.home', compact('categories', 'ville'));
            } else {

                if ($request->ville) {

                    foreach ($prestataires as $key => $prestataire) {
                        $result = Prestataire::where([['ville', $request->ville]])->get();
                        foreach ($result as $key => $value) {
                            if ($value->id == $prestataire->prestataire_id) {
                                $results = Prestataire::where([['id', $prestataire->prestataire_id]])->get();
                            }
                        }
                    }
                } else {
                    $results = $prestataires;
                }

                $ville = $request->ville;
                return view('client.home', compact('categories', 'prestataires', 'ville', 'results'));
            }
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
        }
    }

    public function rechercherPrestataire(Request $request)
    {
        try {
            $service = Service::where('categories_id', $request->categories_id)->where('titreService', $request->service)->get();

            $categories = Categories::with('souscategorie')->get();

            if (count($service) < 0) {
                $reponse = "Oupps...!!! Aucun resultat  pour cette recherche";
                return view('client.categories', compact('categories', 'reponse'));
            }

            // $prix = explode(",", $request->prix);


            foreach ($service as $key => $value) {

                $results = DB::table('prestataires')
                    ->join('categories_prestataire', 'prestataires.id', '=', 'categories_prestataire.prestataire_id')
                    ->where('categories_id', $value->categories_id)
                    // ->whereBetween('prix_max', [$prix[0], $prix[1]])
                    ->paginate(8);
            }

            if (empty($results)) {
                $reponse = "Oupps...!!!  Aucun resultat  pour cette recherche";
                return view('client.categories', compact('categories', 'reponse'));
            } else {
                foreach ($results as $key => $prestataire) {
                    $result = Prestataire::where([['ville', $request->ville]])->get();
                    foreach ($result as $key => $value) {
                        if ($value->id == $prestataire->prestataire_id) {
                            $prestataires = Prestataire::where([['id', $prestataire->prestataire_id]])->paginate(8);
                        }
                    }
                }

                $reponse = "Résultats pour cette recherche";
                return view('client.categories', compact('categories', 'prestataires', 'reponse'));
            }
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
        }
    }

    public function envoyerMessage(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'subject' => 'required',
                'message' => 'required'
            ]);

            ContactUs::create($request->all());

            Mail::send([], array(
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'subject' => $request->get('subject'),
                'message' => $request->get('message'),
            ), function ($message) use ($request) {
                $message->from($request->email, $request->get('message'));
                $message->to('contact@afrik-solutions.com', 'Bonjour l\'admin')->subject($request->get('subject'));
            });

            return back()->with('success', 'Votre message a bien ete envoyer');
        } catch (Exception $e) {
            echo 'and the error is: ',  $e->getMessage(), "\n";
        }
    }

    public function clickContact($id)
    {
        try {
            Prestataire::where('id', $id)->update(array(
                'click_contact' => $id + 1,
            ));

            return response()->json([
                'success' =>  "Ok!",
            ]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()]);
        }
    }
    public function chercherSousCategorie($id)
    {
        try {
            $sousCategories = SousCategorie::where('categories_id', $id)->get();
            return response()->json(['array' =>  $sousCategories]);
        } catch (Exception $e) {
            return response()->json(['error' =>  $e->getMessage()], 500);
        }
    }
}
