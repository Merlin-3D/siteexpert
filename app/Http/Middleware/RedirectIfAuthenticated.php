<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (auth()->user()->hasRole('ADMIN')) { // do your magic here
                return redirect()->route('admin.accueil_admin');
            } elseif (auth()->user()->hasRole('PRESTATAIRE')) {
                return redirect()->route('prestataires.accueil');
            }
            elseif (auth()->user()->hasRole('CLIENT')) {
                return redirect()->route('/');
            }
        }

        return $next($request);
    }
}
