<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                [
                    'id'=>"1",
                    'name' => "ADMIN",
                ],
                [
                    'id'=>"2",
                    'name' => "PRESTATAIRE",
                ],
                [
                    'id'=>"3",
                    'name' => "CLIENT",
                ],
            ]

        );

        DB::table('role_user')->insert(
            [
            [
                'role_id' => "1",
                'user_id' => "1"
            ],

        ]
    );
    }
}
