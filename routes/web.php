<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('/');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'ClientController@acceuil')->name('/');
Route::get('/contact', 'ClientController@contact')->name('page_contact');
Route::get('/prestataire/{id}', 'ClientController@prestataire')->name('page_prestataire');
Route::get('/ville/{nom}', 'ClientController@ville')->name('page_ville');
Route::get('/categorie/{nom}', 'ClientController@categorie')->name('page_categorie');
Route::get('/devenir', 'ClientController@devenirFreelancer')->name('page_devenir');
Route::post('/creerClient', 'ClientController@creerClient')->name('creer_client');

Route::prefix('client')->name('client.')->group(function () {
    Route::post('/ajouter/commentaire', 'ClientController@ajouterCommentaire')->name('ajouter_commentaire');
    Route::post('/Envoyer/contact', 'ClientController@envoyerMessage')->name('envoyer_contact');
    Route::post('/rechercher/accueil', 'ClientController@rechercheAccueil')->name('rechercher_accueil');
    Route::post('/rechercher/prestataire', 'ClientController@rechercherPrestataire')->name('rechercher_prestataire');
    Route::get('/count/clickContact/{id}', 'ClientController@clickContact')->name('click_contact');
    Route::get('/chercher/souscompetence/{id}', 'ClientController@chercherSousCategorie')->name('sous_categorie_client');

});

Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/config/admin', 'AdminController@index')->name('accueil_admin');
    Route::post('/config/ajouterAdmin', 'AdminController@ajouterAdmin')->name('ajouter_admin');
    Route::get('/config/AjouterAdministrateur', 'AdminController@compteAdmin')->name('config_administration');
    Route::get('/config/supprimerAdmin/{id}', 'AdminController@supprimerAdmin')->name('supprimer_admin');
    Route::get('/config/categorie', 'AdminController@acceuilCategorie')->name('config_categorie');
    Route::post('/config/ajouterCategorie', 'AdminController@ajouterCategorie')->name('ajouter_categorie');
    Route::get('/config/supprimerCategorie/{id}', 'AdminController@supprimerCategorie')->name('supprimer_categorie');
    Route::get('/config/prestataire', 'AdminController@prestataire')->name('config_prestataire');
    Route::get('/config/desactiverPrestataire/{user_id}/{value}', 'AdminController@desactiverPrestataire')->name('descativer_prestataire');
    Route::post('/config/ajouterPrestataire', 'AdminController@ajouterPrestataire')->name('ajouter_prestataire');
    Route::get('/config/supprimerPrestataire/{id}/{user_id}', 'AdminController@supprimerPrestataire')->name('supprimer_prestataire');
    Route::get('/config/commentaire', 'AdminController@commentaire')->name('config_commentaire');
    Route::get('/config/supprimerCommentaire/{id}', 'AdminController@supprimerCommentaire')->name('supprimer_commentaire');
});

Route::prefix('prestataires')->name('prestataires.')->group(function () {
    Route::get('/accueil/{id}', 'PrestataireController@index')->name('accueil');
    Route::get('/profil/{id}', 'PrestataireController@profilPrestataire')->name('profil');
    Route::post('/modifier/profil', 'PrestataireController@modifierProfil')->name('modifier_profil');
    Route::get('/competence/{id}', 'PrestataireController@competence')->name('competence');
    Route::get('/chercher/souscompetence/{id}', 'PrestataireController@chercherSousCategorie')->name('sous_categorie');
    Route::post('/ajouter/competence', 'PrestataireController@ajouterCompetence')->name('ajouter_competence');
    Route::get('/supprimer/competence/{categories_id}/{prestataire_id}/{id}', 'PrestataireController@supprimerCompetence')->name('supprimer_competence');
    Route::get('/entreprise/{id}', 'PrestataireController@entreprise')->name('entreprise');
    Route::post('/ajouter/entreprise', 'PrestataireController@ajouterEntreprise')->name('ajouter_entreprise');
    Route::get('/supprimer/entreprise/{entreprise_id}', 'PrestataireController@supprimerEntreprise')->name('supprimer_entreprise');
    Route::get('/realisation/{id}/{titre_categ}/{categorie_id}', 'PrestataireController@realisation')->name('realisation');
    Route::post('ajouter/realisation', 'PrestataireController@ajouterRealisation')->name('ajouter_realisation');
    Route::get('/supprimer/realisation/{id}', 'PrestataireController@supprimerRealisation')->name('supprimer_realisation');
    Route::post('modifier/banniere', 'PrestataireController@uploadBanner')->name('modifier_banner');

});
