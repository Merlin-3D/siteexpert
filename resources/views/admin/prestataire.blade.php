@extends('layouts.app')

@section('css')


@endsection

@section('title')
    <i class="icon-line-awesome-street-view"></i> Prestataires
@endsection

@section('subtitle')
    Gestion des Prestataires
@endsection

@section('menu')
    Prestataires
@endsection

@section('content')
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <a href="#sign-in-dialog" class="button popup-with-zoom-anim log-in-button"><i
                            class="icon-feather-plus"></i>
                        Ajouter un prestataire</a>
                    <div id="message"></div>
                </div>

                <div class="content pb-2 margin-top-30">
                    <table class="basic-table mb-5 container mt-5" id="example">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Prénom</th>
                                <th scope="col">Email</th>
                                <th scope="col">Télèphone</th>
                                <th scope="col">Age</th>
                                <th scope="col">Sexe</th>
                                <th scope="col">Whatsapp</th>
                                <th scope="col">Vues</th>
                                <th scope="col">Contact vue</th>
                                <th scope="col">Statut</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($prestataires as $prestataire)
                                <tr id="prestataire_{{ $prestataire->id }}">
                                    <td scope="row">{{ $prestataire->id }}</td>
                                    <td>{{ $prestataire->nom }}</td>
                                    <td>{{ $prestataire->prenom }}</td>
                                    <td>{{ $prestataire->email }}</td>
                                    <td>{{ $prestataire->telephone }}</td>
                                    <td>{{ $prestataire->age }}</td>
                                    <td>{{ $prestataire->sexe }}</td>
                                    <td>{{ $prestataire->whatsapp }}</td>
                                    <td>{{ $prestataire->click ?$prestataire->click:0 }}</td>
                                    <td>{{ $prestataire->click_contact }}</td>
                                    <td>
                                        @if ($prestataire->user->status == "1")
                                                   <button class="button green"
                                            onclick="desactiverPrestataire({{ $prestataire->user_id }}, 0)"><i
                                                class="icon-material-outline-power-settings-new"></i></button>

                                        @else
                                        <button class="button  red"
                                        onclick="desactiverPrestataire({{ $prestataire->user_id }}, 1)"><i
                                            class="icon-material-outline-power-settings-new"></i></button>

                                        @endif
                                    </td>
                                    <td>
                                        <button class="button red"
                                            onclick="supprimerprestataire({{ $prestataire->id }}, {{ $prestataire->user_id }})"><i
                                                class="icon-feather-trash-2"></i></button>
                                    </td>

                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="dashboard-footer-spacer"></div>

    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

        <!--Tabs -->
        <div class="sign-in-form">

            <ul class="popup-tabs-nav">
                <li><a href="#login">Ajouter un prestataire</a></li>
            </ul>

            <div class="popup-tabs-container">

                <!-- Login -->
                <div class="popup-tab-content">
                    <div id="notification"></div>
                    <form method="POST" id="ajout-prestataire-form">
                        @csrf

                        <div class="col-xl-12">
                            <div class="submit-field">
                                <h5>Nom</h5>
                                <input id="name" type="text" class="input-text with-border" name="nom"
                                    id="emailaddress-register" placeholder="Entrer un nom" />
                                <span class="text-danger" id="nom-error"></span>

                            </div>
                        </div>

                        <div class="col-xl-12">
                            <div class="submit-field">
                                <h5>Prénom</h5>
                                <input id="name" type="text" class="input-text with-border" name="prenom"
                                    id="emailaddress-register" placeholder="Entrer un prénom" />
                                <span class="text-danger" id="prenom-error"></span>

                            </div>
                        </div>

                        <div class="col-xl-12">
                            <div class="submit-field">
                                <h5>Email</h5>
                                <input type="email" class="input-text with-border" name="email" id="emailaddress-register"
                                    placeholder="Entrer une adresse mail" />
                                <span class="text-danger" id="email-error"></span>

                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="submit-field">
                                <h5>Mot de passe</h5>
                                <input type="password" class="input-text with-border " name="password"
                                    name="password-register" placeholder="Entrer un mot de passe" />
                                <span class="text-danger" id="password-error"></span>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Ajouter</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#ajout-prestataire-form').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $('#nom-error').text('');
            $('#prenom-error').text('');
            $('#email-error').text('');
            $('#password-error').text('');

            var notification = document.getElementById('notification')

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.ajouter_prestataire') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        notification.innerHTML =
                            `<div class="notification success closeable mt-3">
                            <p> ${response.message} </p>
                            <a class="close" href="#"></a>
                        </div>`

                        $('table tbody').append(`
                        <tr id="prestataire_${ response.prestataire.id }">
                                    <td scope="row">${response.prestataire.id }</td>
                                    <td>${ response.prestataire.nom }</td>
                                    <td>${ response.prestataire.prenom }</td>
                                    <td>${ response.prestataire.email }</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>
                                        <button class="button green"
                                            onclick="desactiverPrestataire(${ response.prestataire.user_id }, 0)"><i
                                                class="icon-material-outline-power-settings-new"></i></button>
                                    </td>
                                    <td>
                                        <button class="button red" onclick="supprimerprestataire(${ response.prestataire.id },${ response.prestataire.user_id })"><i class="icon-feather-trash-2"></i></button>
                                    </td>

                                </tr>
                        `)

                        this.reset();
                    }
                },
                error: function(response) {
                    console.log(response.responseJSON)
                    $('#nom-error').text(response.responseJSON.errors.nom);
                    $('#prenom-error').text(response.responseJSON.errors.prenom);
                    $('#email-error').text(response.responseJSON.errors.email);
                    $('#password-error').text(response.responseJSON.errors.password);
                }
            });

        });

        function supprimerprestataire(id, user_id) {
            var message = document.getElementById('message')

            url = "/admin/config/supprimerPrestataire/" + id + "/" + user_id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    if (response) {
                        $("#prestataire_" + id).remove();
                        alert(response.success)
                    }
                }
            });
        }

        function desactiverPrestataire(user_id, value) {
            url = "/admin/config/desactiverPrestataire/" + user_id + "/" + value;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    if (response) {
                       window.location.reload();
                    }
                }
            });
        }
    </script>
@endsection
