@extends('layouts.app')

@section('css')
@endsection
@section('title')
<i class="icon-material-outline-supervisor-account"></i>Comptes -- Administrateur
@endsection

@section('subtitle')
    Gestion Des Comptes
@endsection

@section('menu')
    Comptes
@endsection

@section('content')
<div class="row">

    <!-- Dashboard Box -->
    <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

            <!-- Headline -->
            <div class="headline">

                    <div id="message">
                        <a href="#sign-in-dialog" class="button popup-with-zoom-anim log-in-button"><i
                            class="icon-feather-plus"></i>
                        Ajouter un Administrateur</a>
                    </div>
            </div>

            <div class="content pb-2">
                <table class=" basic-table mb-5 container mt-5" id="example">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-center" scope="col">id</th>
                            <th class="text-center" scope="col">Nom</th>
                            <th class="text-center" scope="col">Email</th>
                            <th class="text-center" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr  id="user_{{ $user->id }}">
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <button class="button red" onclick="supprimerAdmin({{ $user->id }})"><i class="icon-feather-trash-2"></i></button>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

    <div class="dashboard-footer-spacer"></div>

    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

        <!--Tabs -->
        <div class="sign-in-form">

            <ul class="popup-tabs-nav">
                <li><a href="#register">Ajouter un Administrateur</a></li>
            </ul>

            <div class="popup-tabs-container">

                <!-- Register -->
                <div class="popup-tab-content" id="register">
                    <div id="notification"></div>

                    <!-- Welcome Text -->

                    <!-- Form -->
                    <form  id="ajout-admin-form">
                        @csrf
                        <div class="input-with-icon-left">
                            <i class="icon-feather-user"></i>
                            <input type="text" class="input-text with-border @error('name') is-invalid @enderror" placeholder="Votre Nom" name="name" value="{{ old('name') }}"  autocomplete="name" />
                            <span class="text-danger" id="name-error"></span>

                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline"></i>
                            <input type="email" class="input-text with-border @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Adresse e-mail"  autocomplete="email"/>
                            <span class="text-danger" id="email-error"></span>
                            </div>

                        <div class="input-with-icon-left" title="Should be at least 8 characters long"
                            data-tippy-placement="bottom">
                            <i class="icon-material-outline-lock"></i>
                            <input type="password" class="input-text with-border @error('password') is-invalid @enderror" name="password"  placeholder="Mot de passe" autocomplete="new-password"/>
                            <span class="text-danger" id="password-error"></span>
                        </div>
                        <button class="margin-top-10 button full-width button-sliding-icon ripple-effect" type="submit"
                            >Ajouter <i
                                class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>


                </div>

            </div>
        </div>
    </div>

@endsection
@section('script')
<script>
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#ajout-admin-form').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $('#name-error').text('');
            $('#email-error').text('');
            $('#password-error').text('');

            var notification = document.getElementById('notification')

            $.ajax({
                type: 'POST',
                url: "{{ route('admin.ajouter_admin') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        console.log(response)
                        notification.innerHTML =
                            `<div class="notification success closeable mt-3">
                            <p> ${response.success} </p>
                            <a class="close" href="#"></a>
                        </div>`

                        $('table tbody').append(`
                                <tr id="user_${ response.users.id }">
                                <td>${ response.users.id }</td>
                                <td>${ response.users.name }</td>
                                <td>${ response.users.email }</td>
                                <td>
                                    <button class="button red" onclick="supprimerAdmin(${ response.users.id })"><i class="icon-feather-trash-2"></i></button>
                                </td>
                            </tr>
                        `)

                      //  this.reset();
                    }
                },
                error: function(response) {
                    console.log(response.responseJSON)
                    $('#name-error').text(response.responseJSON.errors.name);
                    $('#email-error').text(response.responseJSON.errors.email);
                    $('#password-error').text(response.responseJSON.errors.password);
                }
            });

        });
        function supprimerAdmin(id) {
           // var message = document.getElementById('message')

            url = "/admin/config/supprimerAdmin/" + id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    if (response) {
                        $("#user_" + id).remove();
                        alert(response.success)
                    }
                }
            });
        }
</script>
@endsection
