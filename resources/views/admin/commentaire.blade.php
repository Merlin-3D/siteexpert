@extends('layouts.app')

@section('css')
@endsection
@section('title')
<i class="icon-material-outline-rate-review"></i> Commentaires
@endsection

@section('subtitle')
    Gestion Commentaires
@endsection

@section('menu')
    Commentaires
@endsection

@section('content')
<div class="row">

    <!-- Dashboard Box -->
    <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">

            <!-- Headline -->
            <div class="headline">
                <span  class=" popup-with-zoom-anim log-in-button"><i class="icon-material-outline-rate-review"></i>
                    Ajouter un prestataire</span>
                    <div id="message"></div>
            </div>

            <div class="content pb-2">
                <table class=" basic-table mb-5 container mt-5" id="example">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-center" scope="col">Client</th>
                            <th class="text-center" scope="col">Email</th>
                            <th class="text-center" scope="col">Commentaire</th>
                            <th class="text-center" scope="col">Destinataire</th>
                            <th  scope="col">Posté le.</th>
                            <th class="text-center" scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($commentaires as $commentaire)
                            <tr id="commentaire_{{ $commentaire->id }}">
                                <td>{{ $commentaire->client->nom }}</td>
                                <td>{{ $commentaire->client->email }}</td>
                                <td> <p class="text-justify">{{ $commentaire->commentaire }}</p></td>
                                <td>{{ $commentaire->prestataire->nom }} {{ $commentaire->prestataire->prenom }}</td>
                                <td>{{ $commentaire->created_at }}</td>
                                <td>
                                    <button class="button red" onclick="supprimerCommentaire({{ $commentaire->id }})"><i class="icon-feather-trash-2"></i></button>
                                </td>

                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

    <div class="dashboard-footer-spacer"></div>

@endsection
@section('script')
<script>
    function supprimerCommentaire(id) {
            var message = document.getElementById('message')

            url = "/admin/config/supprimerCommentaire/" + id ;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    if (response) {
                        $("#commentaire_" + id).remove();
                        alert(response.success)
                    }
                }
            });
        }
</script>
@endsection
