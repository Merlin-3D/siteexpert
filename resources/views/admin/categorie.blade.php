@extends('layouts.app')

@section('css')
@endsection
@section('title')
    <i class="icon-material-outline-dashboard"></i> Catégories
@endsection

@section('subtitle')
    Gestion des catégories
@endsection

@section('menu')
    Catégories
@endsection

@section('content')
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <a href="#sign-in-dialog" class="button popup-with-zoom-anim log-in-button"><i
                            class="icon-feather-plus"></i>
                        Ajouter une catègorie</a>

                </div>

                <div class="content" >
                    <ul class="dashboard-box-list">
                        @foreach ($categories as $categorie)
                            <li id="categorie_{{ $categorie->id }}" >
                                <!-- Job Listing -->
                                <div class="job-listing width-adjustment">

                                    <!-- Job Listing Details -->
                                    <div class="job-listing-details">

                                        <!-- Details -->
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title"><a href="#">{{ $categorie->titre }}</a></h3>
                                        </div>
                                    </div>
                                </div>

                                <!-- Task Details -->
                                @if (count($categorie->souscategorie)>0)
                                   <ul class="dashboard-task-info">
                                    <li>
                                        @foreach ($categorie->souscategorie as $souscategorie)
                                          <span class="dashboard-status-button green"> {{ $souscategorie->titre_sous_categorie }} </span>
                                        @endforeach
                                    </li>
                                </ul>
                                @endif

                                <!-- Buttons -->
                                <div class="buttons-to-right always-visible">
                                    <button onclick="supprimerCategorie({{ $categorie->id }})"
                                        class="button red ripple-effect ico" title="Cancel Bid"
                                        data-tippy-placement="top"><i class="icon-feather-trash-2"></i></button>
                                </div>
                            </li>
                        @endforeach


                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-footer-spacer"></div>

    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

        <!--Tabs -->
        <div class="sign-in-form">

            <ul class="popup-tabs-nav">
                <li><a href="#login">Ajouter une catégorie</a></li>
            </ul>

            <div class="popup-tabs-container">

                <!-- Login -->
                <div class="popup-tab-content">
                    <div id="notification"></div>
                    <!-- Form -->

                    <div class="col-xl-12">
                        <div class="submit-field">
                            <h5>Nom de la catégorie</h5>
                            <input type="text" name="titre" id="titre" class="with-border"
                                placeholder="Titre de la catégorie">
                            <span class="text-danger" id="titre-error"></span>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="submit-field">
                            <h5>Choisir une ou plusieurs sous-catégorie</h5>

                            <select id="sous_cat" name="sous_cat" style="height: 200px" multiple
                                data-selected-text-format="count > 1">
                                <option>Laravel</option>
                                <option>NodeJs</option>
                                <option>Spring Boot</option>
                                <option>Synfony</option>
                                <option>Flutter</option>
                                <option>Angular</option>
                                <option>ReactJs</option>
                                <option>React Native</option>
                                <option>Django</option>
                                <option>WordPress</option>
                            </select>
                        </div>
                    </div>


                    <!-- Button -->
                    <button onclick="ajouterCategorie()" class="button full-width button-sliding-icon ripple-effect">Ajouter
                        <i class="icon-material-outline-arrow-right-alt"></i></button>

                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function ajouterCategorie() {
            var titre = $('#titre').val()
            $('#titre-error').text('');

            var sous_cat = $('#sous_cat').val()
            var notification = document.getElementById('notification')
            $.ajax({
                url: "{{ route('admin.ajouter_categorie') }}",
                type: "POST",
                contentType: 'application/x-www-form-urlencoded',
                data: {
                    "_token": '{{ csrf_token() }}',
                    titre: titre,
                    sous_cat: sous_cat
                },
                dataType: 'JSON',
                success: function(response) {
                    notification.innerHTML =
                            `<div class="notification success closeable mt-3">
                            <p> ${response.message} </p>
                            <a class="close" href="#"></a>
                        </div>`
                       window.location.reload();

                },
                error: function(err) {
                    $('#titre-error').text(err.responseJSON.errors.titre);
                }
            });
        }

        function supprimerCategorie(id) {

            url = "/admin/config/supprimerCategorie/" + id

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    if (response) {
                        alert(response.success)
                        $("#categorie_" + id).remove();
                    }
                }
            });
        }
    </script>
@endsection
