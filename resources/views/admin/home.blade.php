@extends('layouts.app')

@section('title')
    <i class="icon-line-awesome-dashboard"></i> Administration -- {{ Auth::user()->name }}
@endsection

@section('subtitle')
    Tableau de bord
@endsection

@section('menu')
    Accueil
@endsection


@section('content')
    <div class="fun-facts-container">
        <div class="fun-fact" data-fun-fact-color="#efa80f">
            <div class="fun-fact-text">
                <span>Prestataires</span>
                <h4> {{ $prestataire }}</h4>
            </div>
            <div class="fun-fact-icon"><i class="icon-line-awesome-street-view"></i></div>
        </div>
        <div class="fun-fact" data-fun-fact-color="#36bd78">
            <div class="fun-fact-text">
                <span>Clients</span>
                <h4>{{ $client }}</h4>
            </div>
            <div class="fun-fact-icon"><i class="icon-line-awesome-group
                "></i></div>
        </div>
        {{-- <div class="fun-fact" data-fun-fact-color="#b81b7f">
        <div class="fun-fact-text">
            <span>Réalisations</span>
            <h4>4</h4>
        </div>
        <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
    </div> --}}
        <!-- Last one has to be hidden below 1600px, sorry :( -->
        <div class="fun-fact" data-fun-fact-color="#2a41e6">
            <div class="fun-fact-text">
                <span>Catégories</span>
                <h4>{{ $categorie }}</h4>
            </div>
            <div class="fun-fact-icon"><i class="icon-line-awesome-inbox"></i></div>
        </div>
    </div>
@endsection
