<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
                <span class="hamburger hamburger--collapse">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </span>
                <span class="trigger-title">Dashboard Navigation</span>
            </a>

            <!-- Navigation -->
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Menu">
                        @if (auth()->user()->hasRole('PRESTATAIRE'))
                            <li class="active"><a href="{{ route('prestataires.accueil', Auth::user()->id) }}"><i
                                        class="icon-material-outline-dashboard"></i>
                                    Dashboard</a></li>
                            <li><a href="{{ route('prestataires.profil', Auth::user()->id) }}"><i
                                        class="icon-material-outline-person-pin"></i>
                                    Profil</a></li>
                            <li><a href="{{ route('prestataires.competence', Auth::user()->id) }}"><i class="icon-line-awesome-puzzle-piece"></i>
                                    Compétences</a></li>
                            {{-- <li><a href="dashboard-reviews.html"><i class="icon-material-outline-business-center"></i>
                                    Réalisation</a></li> --}}
                            <li><a href="{{ route('prestataires.entreprise', Auth::user()->id) }}"><i class="icon-material-outline-business"></i>
                                    Entreprise</a></li>
                            {{-- <li><a href="dashboard-reviews.html"><i class="icon-line-awesome-bar-chart"></i>
                                    Statistique</a></li> --}}

                        @endif
                        @if (auth()->user()->hasRole('ADMIN'))
                            {{-- <li><a href="dashboard-reviews.html"><i class="icon-line-awesome-key"></i> Acl</a></li> --}}
                            <li class="active"><a href="{{ route('admin.accueil_admin') }}"><i
                                class="icon-line-awesome-dashboard
                                "></i>
                            Dashboard</a></li>
                            <li><a href={{ route('admin.config_administration') }}><i
                                        class="icon-material-outline-supervisor-account"></i> Administrateur</a></li>
                            <li><a href="{{ route('admin.config_categorie') }}"><i class="icon-material-outline-dashboard"></i>
                                    Catégories</a>
                            </li>
                            <li><a href="{{ route('admin.config_prestataire') }}"><i class="icon-line-awesome-street-view"></i>
                                    Prestataires</a></li>
                            <li><a href="{{ route('admin.config_commentaire') }}"><i class="icon-material-outline-rate-review"></i>
                                    Commentaires</a></li>

                            {{-- <li><a href="dashboard-reviews.html"><i class="icon-line-awesome-users"></i>
                                    Utilisateurs</a></li> --}}

                        @endif

                    </ul>

                </div>
            </div>
            <!-- Navigation / End -->

        </div>
    </div>
</div>
