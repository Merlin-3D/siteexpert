@extends('layouts.app')

@section('title')
    <i class="icon-material-outline-business-center"></i> Réalisations -- {{ $titre_categ }}
@endsection

@section('subtitle')
    Ajouter de nouvelles réalisations
@endsection

@section('menu')
    {{ $titre_categ }}
@endsection

@section('content')
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12 ">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <a href="#sign-in-dialog" class="button popup-with-zoom-anim log-in-button"><i
                            class="icon-feather-plus"></i>
                        Nouvelle Réalisation</a>
                    <div class="notification notice closeable mt-2">
                        <p>Vous ne pouvez uploader que 4 fichiers </p>
                    </div>
                </div>
                <div class="col-xl-12 col-md-12 margin-top-10 text-center">
                    <table class="basic-table " style="margin-bottom: 10px">

                        <tr>
                            <th class="col-xl-4 col-md-12 text-center">
                                Médias
                            </th>
                            <th class="col-xl-6 col-md-12 text-center">
                                Description
                            </th>
                            <th class="col-xl-12 col-md-12">Action</th>

                        </tr>
                        <tbody class="tbody">
                            @foreach ($realisations as $realisation)
                                <tr id="realisation_{{ $realisation->id }}">
                                    <td data-label="Column 1">
                                        {{-- <img src="{{ asset('images/company-logo-06.png') }}" height="100px" alt=""> --}}
                                        <img src="{{ asset('/realisations/' . $realisation->url) }}" height="100px" alt="">
                                    </td>
                                    <td data-label="Column 2">
                                        {{-- <img src="{{ asset('images/company-logo-06.png') }}" height="100px" alt=""> --}}
                                     <p class="text-justify">{{  $realisation->description }}</p>
                                    </td>
                                    <td data-label="Column 3">
                                        <button class="button red" onclick="supprimerRealisation({{ $realisation->id }})">
                                            <i class="icon-feather-trash-2"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
        <!--Tabs -->
        <div class="sign-in-form">

            <ul class="popup-tabs-nav">
                <li><a href="#login">Nouvelle Réalisation</a></li>
            </ul>

            <div class="popup-tabs-container">

                <!-- Login -->
                <div class="popup-tab-content">
                    <div id="notification"></div>

                    <!-- Form -->
                    <form method="POST" id="upload-file-form" enctype="multipart/form-data">
                        @csrf
                        <div class="uploadButton margin-top-30">
                            <input type="hidden" name="compt_id" id="compt_id" value="{{ $compt_id }}">
                            <input type="hidden" name="categorie_id" id="categorie_id" value="{{ $categorie_id }}">

                            <input class="uploadButton-input" type="file" accept="image/*, application/zip" name="file"
                                id="upload" />
                            <label class="uploadButton-button ripple-effect" for="upload">Ajouter une réalisation</label>
                            <span class="uploadButton-file-name"> Image ou fichier zip pouvant être utiles pour décrire
                                votre travail</span>
                        </div>
                        <span class="text-danger" id="image-input-error"></span>
                        <label for=""></label>
                        <textarea name="description" id="description" cols="30" rows="5"
                        placeholder="Ajouter une description"></textarea>
                        <!-- Button -->
                        <button type="submit" class="button full-width button-sliding-icon ripple-effect">Ajouter <i
                                class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#upload-file-form').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            $('#image-input-error').text('');
            var notification = document.getElementById('notification')

            $.ajax({
                type: 'POST',
                url: "{{ route('prestataires.ajouter_realisation') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        // this.reset();
                        notification.innerHTML =
                            `<div class="notification success closeable mt-3">
                            <p>Réalisation ajouter</p>
                            <a class="close" href="#"></a>
                        </div>`
                        $('.tbody').append(`
                        <tr id="realisation_${ response.array.id }}">
                                <td data-label="Column 1">
                                    <img src='/realisations/${response.array.url}' height="100px" alt="">
                                </td>
                                <td data-label="Column 2">
                                    <p class="text-justify">${  response.array.description }</p>
                                </td>
                                <td data-label="Column 3">
                                    <button class="button red" onclick="supprimerRealisation( ${response.array.id})"> <i
                                            class="icon-feather-trash-2"></i></button>
                                </td>
                            </tr>
                        `)
                    }
                },
                error: function(response) {
                    $('#image-input-error').text(response.responseJSON.errors.file);
                }
            });

        });

        function supprimerRealisation(id) {
            var message = document.getElementById('message')

            url = "/prestataires/supprimer/realisation/" + id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    if (response) {
                        $("#realisation_" + id).remove();
                        alert(response.success)

                    }
                }
            });
        }
    </script>
@endsection
