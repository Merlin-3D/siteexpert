@extends('layouts.app')

@section('title')
    <i class="icon-material-outline-business-center"></i> Entreprises
@endsection

@section('subtitle')
    Ajouter une nouvelle compétences
@endsection

@section('menu')
    Entreprise
@endsection

@section('content')

    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <a href="#sign-in-dialog" class="button popup-with-zoom-anim log-in-button"><i
                            class="icon-feather-plus"></i>
                        Nouvelle Entreprises</a>
                        <div id="message"></div>
                </div>

                <div class="content">
                    <ul class="dashboard-box-list">
                        @foreach ($entreprises as $entreprise)
                            <li id="entreprise_{{ $entreprise->id }}">
                                <div class="job-listing">
                                    <div class="job-listing-details">

                                        <a href="#" class="job-listing-company-logo">
                                            <img src="{{ asset('images/company-logo-06.png') }}" alt="">
                                        </a>
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title"><a href="#"> {{ $entreprise->tache }} </a></h3>

                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li><i class="icon-material-outline-business"></i>
                                                        {{ $entreprise->nom }}</li>
                                                    <li><i class="icon-material-outline-access-time"></i>
                                                        {{ $entreprise->date_publication }}</li>
                                                        <li><i class="icon-feather-phone"></i>
                                                            {{ $entreprise->contact }}</li>
                                                </ul>
                                            </div>
                                            <h3 class="job-listing-title"><a href="#"> {{ $entreprise->temoignage }} </a></h3>

                                        </div>
                                    </div>
                                </div>
                                <div class="buttons-to-right">
                                    <button onclick="supprimerEntreprise({{ $entreprise->id }})"
                                        class="button red ripple-effect ico" title="Remove" data-tippy-placement="left"><i
                                            class="icon-feather-trash-2"></i></button>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
        <!--Tabs -->
        <div class="sign-in-form">

            <div class="popup-tabs-container">

                <!-- Login -->
                <div class="popup-tab-content">
                    <div id="notification"></div>

                    <!-- Form -->
                    <form method="post">
                        <div class="col-xl-12 col-md-12">
                            <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
                            <div class="section-headline margin-top-25 margin-bottom-12">
                                <h5>Nom</h5>
                            </div>
                            <input type="text" id="nom" placeholder="Nom de l'entreprise">
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <div class="section-headline margin-top-25 margin-bottom-12">
                                <h5>Taches</h5>
                            </div>
                            <input type="text" id="tache" placeholder="Taches réalisées">
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <div class="section-headline margin-top-25 margin-bottom-12">
                                <h5>Date de publication</h5>
                            </div>
                            <input type="date" id="date_publication" placeholder="Placeholder">
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <div class="section-headline margin-top-25 margin-bottom-12">
                                <h5>Témoignage</h5>
                            </div>
                            <textarea name="description" id="temoignage" cols="30" rows="5"
                            placeholder="Ajouter un témoignage"></textarea>
                        </div>
                        <div class="col-xl-12 col-md-12">
                            <div class="section-headline margin-top-25 margin-bottom-12">
                                <h5>Contact</h5>
                            </div>
                            <input type="date" id="contact" placeholder="Placeholder">
                        </div>
                    </form>

                    <!-- Button -->
                    <button onclick="ajouterEntreprise()" class="button full-width button-sliding-icon ripple-effect"
                        form="login-form">Ajouter <i class="icon-material-outline-arrow-right-alt"></i></button>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function ajouterEntreprise() {
            var user_id = $('#user_id').val()
            var nom = $('#nom').val()
            var tache = $('#tache').val()
            var date_publication = $('#date_publication').val()
            var temoignage = $('#temoignage').val()
            var contact = $('#contact').val()

            var notification = document.getElementById('notification')


            $.ajax({
                url: "{{ route('prestataires.ajouter_entreprise') }}",
                type: "POST",
                contentType: 'application/x-www-form-urlencoded',
                data: {
                    "_token": '{{ csrf_token() }}',
                    user_id: user_id,
                    nom: nom,
                    tache: tache,
                    date_publication: date_publication,
                    temoignage:temoignage,
                    contact:contact
                },
                dataType: 'JSON',
                success: function(response) {
                    var codeHTML = '';
                    if (response) {

                        notification.innerHTML =
                            `<div class="notification success closeable">
                            <p>${response.success}</p>
                            <a class="close" href="#"></a>
                        </div>`

                            codeHTML =`
                            <li id="entreprise_${ response.array.id }">
                                <div class="job-listing">
                                    <div class="job-listing-details">

                                        <a href="#" class="job-listing-company-logo">
                                            <img src="{{ asset('images/company-logo-06.png') }}" alt="">
                                        </a>
                                        <div class="job-listing-description">
                                            <h3 class="job-listing-title"><a href="#"> ${ response.array.tache } </a></h3>

                                            <div class="job-listing-footer">
                                                <ul>
                                                    <li><i class="icon-material-outline-business"></i>
                                                        ${ response.array.nom }</li>
                                                    <li><i class="icon-material-outline-access-time"></i>
                                                        ${ response.array.date_publication }</li>
                                                        <li><i class="icon-feather-phone"></i>
                                                            ${ response.array.contact } </li>
                                                </ul>
                                            </div>
                                            <h3 class="job-listing-title"><a href="#">${ response.array.temoignage }</a></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="buttons-to-right">
                                    <button onclick="supprimerEntreprise(${ response.array.id })"
                                        class="button red ripple-effect ico" title="Remove" data-tippy-placement="left"><i
                                            class="icon-feather-trash-2"></i></button>
                                </div>
                            </li>
                            `

                        $('.dashboard-box-list').append(codeHTML)
                    }
                },
                error: function(err) {
                    notification.innerHTML = `
                        <div class="notification error closeable">
                            <p>Vérifier tout vos champs !</p>
                            <a class="close" href="#"></a>
                        </div>
                        `
                }
            });
        }

        function supprimerEntreprise(id) {
            var message = document.getElementById('message')

            url = "/prestataires/supprimer/entreprise/" + id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {
                    if (response) {
                        message.innerHTML =
                            `<div class="notification success closeable mt-3">
                            <p>${response.success}</p>
                            <a class="close" href="#"></a>
                        </div>`
                        $("#entreprise_" + id).remove();
                    }
                }
            });
        }
    </script>
@endsection
