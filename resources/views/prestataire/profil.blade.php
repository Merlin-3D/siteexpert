@extends('layouts.app')

@section('title')
    <i class="icon-material-outline-person-pin"></i> {{ Auth::user()->name }}
@endsection

@section('subtitle')
    Mettre a jour votre profil
@endsection

@section('menu')
    Profil
@endsection

@section('content')
    <div class="fun-facts-container mb-5">
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">
                <div class="headline">
                    <a href="#sign-in-dialog" class="button popup-with-zoom-anim log-in-button"><i
                            class="icon-feather-plus"></i>
                       Changer la banniere</a>
                        <div id="message"></div>
                </div>
                <div class="content with-padding padding-bottom-0">
                    {{-- <form action="" method="post" enctype="multipart/form-data">
                        @method('patch')
                        @csrf
                    </form> --}}
                    <div id="message"></div>
                    <form method="POST" id="update-profil-form" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-auto">
                                <div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">
                                    <img class="profile-pic"
                                        src="{{ $prestataire->file ? asset('/avatars/' . $prestataire->file) : asset('images/user-avatar-placeholder.png') }}"
                                        alt="" />
                                        {{ $prestataire->file }}
                                    <div class="upload-button"></div>
                                    <input class="file-upload" id="file"  name="file" type="file" accept="image/*" />
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Nom complet</h5>
                                            <input id="nom" type="text" class="input-text with-border " name="nom"
                                                value="{{ $prestataire->nom }}" placeholder="Nom" required />

                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <input type="hidden" value="{{ Auth::user()->id }}" id="user_id" name="user_id">
                                            <h5>Prénom</h5>
                                            <input id="prenom" type="text" class="input-text with-border" name="prenom"
                                                value="{{ $prestataire->prenom }}" placeholder="Prenom" required />

                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Email</h5>
                                            <input type="email" class="input-text with-border" name="email"
                                                value="{{ $users->email }}" id="email" placeholder="Email Address"
                                                required />

                                        </div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="submit-field">
                                            <h5>Age</h5>
                                            <input type="number" class="input-text with-border" name="age" id="age"
                                                value="{{ $prestataire->age }}" required />

                                        </div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="submit-field">
                                            <h5>Sexe</h5>
                                            <select name="sexe" id="sexe" class="input-text" @error('sexe') is-invalid
                                                @enderror">
                                                <option value="{{ $prestataire->age }}" selected disabled hidden>--
                                                    {{ $prestataire->sexe }} --</option>
                                                <option value="M">M</option>
                                                <option value="F">F</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Télèphone</h5>
                                            <input id="telephone" type="number" class="input-text with-border  "
                                                name="telephone" value="{{ $prestataire->telephone }}" required />

                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Whatsapp</h5>
                                            <input id="whatsapp" type="number" class="input-text with-border"
                                                name="whatsapp" value="{{ $prestataire->whatsapp }}" required />
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Ville</h5>
                                            <input id="ville" type="text" class="input-text with-border" name="ville"
                                                value="{{ $prestataire->ville }}" required />

                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Quartier</h5>
                                            <input id="quartier" type="text" class="input-text with-border" name="quartier"
                                                value="{{ $prestataire->quartier }}" required />

                                        </div>
                                    </div>
                                    <div class="col-xl-12">
                                        <div class="submit-field">
                                            <h5>Description</h5>
                                            <textarea cols="30" id="description" name="description" rows="5"
                                                class="with-border">{{ $prestataire->description }}</textarea>
                                        </div>
                                    </div>
                                    {{-- <div class="col-xl-12">
                                        <div class="submit-field">
                                            <h5>Taux horaire</h5>
                                            <input id="taux_horaire" type="number" class="input-text with-border" name="taux_horaire"
                                                value="{{ $prestataire->taux_horaire }}" required />

                                        </div>
                                    </div> --}}
                                    <div class="mb-5" style="text-align: center">
                                        <button type="submit" class="button">Mettre a jour</button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
        <!--Tabs -->
        <div class="sign-in-form">

            <div class="popup-tabs-container">

                <!-- Login -->
                <div class="popup-tab-content">
                    <div id="notification"></div>

                    <!-- Form -->
                    <form method="POST" id="upload-banner-form" enctype="multipart/form-data">
                        @csrf
                        <div class="uploadButton margin-top-30">
                            <input type="hidden" value="{{ Auth::user()->id }}" id="user_id" name="user_id">

                            <input class="uploadButton-input" type="file" accept="image/*" name="file"
                                id="upload" />
                            <label class="uploadButton-button ripple-effect" for="upload">Ajouter une banniere</label>
                            <span class="uploadButton-file-name"> Uploader une banniere</span>
                        </div>
                        <span class="text-danger" id="image-input-error"></span>

                        <!-- Button -->
                        <button type="submit" class="button full-width button-sliding-icon ripple-effect">Ajouter <i
                                class="icon-material-outline-arrow-right-alt"></i></button>
                    </form>

            </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>

$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#update-profil-form').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            const success = document.getElementById('success')

            $.ajax({
                type: 'POST',
                url: "{{ route('prestataires.modifier_profil') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        message.innerHTML =
                            "<div class='alert alert-success' role='alert'>" + response
                            .message + "</div>"
                    }
                },
                error: function(response) {
                    $('#image-input-error').text(response.responseJSON.errors.file);
                }
            });

        });

        $('#upload-banner-form').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);
            const success = document.getElementById('notification')

            $.ajax({
                type: 'POST',
                url: "{{ route('prestataires.modifier_banner') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        success.innerHTML =
                            "<div class='alert alert-success' role='alert'>" + response
                            .message + "</div>"
                    }
                },
                error: function(response) {
                    $('#image-input-error').text(response.responseJSON.errors.file);
                }
            });

        });

        function modifierProfil() {
            var user_id = $('#user_id').val()
            var nom = $('#nom').val()
            var prenom = $('#prenom').val()
            var email = $('#email').val()
            var age = $('#age').val()
            var sexe = $('#sexe').val()
            var ville = $('#ville').val()
            var quartier = $('#quartier').val()
            var description = $('#description').val()
            var telephone = $('#telephone').val()
            var whatsapp = $('#whatsapp').val()
            var file = $('#file').val()

            const success = document.getElementById('success')

            $.ajax({
                url: "{{ route('prestataires.modifier_profil') }}",
                type: "POST",
                contentType: 'application/x-www-form-urlencoded',
                data: {
                    "_token": '{{ csrf_token() }}',
                    user_id: user_id,
                    nom: nom,
                    prenom: prenom,
                    email: email,
                    age: age,
                    sexe: sexe,
                    ville: ville,
                    quartier: quartier,
                    description: description,
                    telephone: telephone,
                    whatsapp: whatsapp,
                    file: file
                },
                dataType: 'JSON',
                success: function(response) {
                    if (response) {
                        message.innerHTML =
                            "<div class='alert alert-success' role='alert'>" + response
                            .message + "</div>"
                    }
                },
                error: function(err) {
                    console.log(err)
                }
            });
        }
    </script>
@endsection
