@extends('layouts.app')

@section('title')
    <i class="icon-line-awesome-puzzle-piece"></i> Compétences
@endsection

@section('subtitle')
    Définir de nouvelle compétences
@endsection

@section('menu')
    Compétences
@endsection

@section('content')
    <div class="row">

        <!-- Dashboard Box -->
        <div class="col-xl-12">
            <div class="dashboard-box margin-top-0">

                <!-- Headline -->
                <div class="headline">
                    <a href="#sign-in-dialog" class="button popup-with-zoom-anim log-in-button"><i
                            class="icon-feather-plus"></i>
                        Nouvelle compétence</a>

                    <div id="message"></div>
                    <hr>
                    <div class="contenu">
                        <ul class="dashboard-box-list">


                            @foreach ($categories as $categorie)
                                @foreach ($categorie->prestataire as $prest)

                                    @if ($prest->user_id == Auth::user()->id)
                                        <li id="categorie_{{ $categorie->id }}">
                                            <!-- Overview -->
                                            <div class="freelancer-overview manage-candidates">
                                                <div class="freelancer-overview-inner">

                                                    <!-- Avatar -->
                                                    <div class="freelancer-avatar">
                                                        <a onclick="supprimerCompetence({{ $categorie->id }},{{ $prest->id }},{{ $prest->pivot->id }})"
                                                            class="button text-white red ripple-effect ico"
                                                            title="Supprimer" data-tippy-placement="top"><i
                                                                class="icon-feather-trash-2"></i></a>
                                                        <a href="{{ route('prestataires.realisation', [$prest->pivot->id, $categorie->titre, $categorie->id]) }}"
                                                            class="button  ripple-effect ico" title="Réalisations"
                                                            data-tippy-placement="top"><i
                                                                class="icon-material-outline-business-center"></i></a>
                                                    </div>

                                                    <!-- Name -->
                                                    <div class="freelancer-name">
                                                        <h4><a href="#">{{ $categorie->titre }} <img class="flag"
                                                                    src="images/flags/de.svg" alt="" title="Germany"
                                                                    data-tippy-placement="top"></a></h4>

                                                        <!-- Bid Details -->
                                                        {{-- <ul class="dashboard-task-info bid-info">
                                                            @foreach ($categorie->prestataire as $tarif)
                                                                <li><strong>{{ $prest->pivot->prix_min }}
                                                                        XAF</strong><span>Tarif Min</span></li>
                                                                <li><strong>{{ $prest->pivot->prix_max }}
                                                                        XAF</strong><span>Tarif Max</span></li>
                                                            @endforeach
                                                        </ul> --}}
                                                        <!-- Buttons -->
                                                        <div
                                                            class="buttons-to-right always-visible margin-top-25 margin-bottom-0">
                                                            @foreach ($categorie->service as $service)
                                                                @if ($prestataire == $prest->pivot->prestataire_id && $categorie->id == $prest->pivot->categories_id)

                                                                    <span
                                                                        class="dashboard-status-button green" style="width: 200px">{{ $service->titreService }}
                                                                        <div class="progress">
                                                                            <div class="progress-bar" role="progressbar" style="width: {{ $service->niveau }}0%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="10">{{ $service->niveau }}</div>
                                                                          </div>
                                                                    </span>

                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                    @endif

                                @endforeach
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">
            <!--Tabs -->
            <div class="sign-in-form">

                <ul class="popup-tabs-nav">
                    <li><a href="#login">Définir une compétence</a></li>
                </ul>

                <div class="popup-tabs-container">

                    <!-- Login -->
                    <div class="popup-tab-content">
                        <div id="notification"></div>

                        <!-- Form -->
                        <form method="post">
                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>Choisir une compétence</h5>
                                    <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
                                    <select class=" input-text with-border" id="titre" style="height: 55px">
                                        <option>-- Choisir --</option>
                                        @foreach ($categories as $categorie)
                                            <option value="{{ $categorie->id }}">{{ $categorie->titre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-12 col-md-12">
                                <div class="section-headline margin-bottom-12">
                                    <h5>Choisir vos services</h5>
                                </div>

                                <select id="sousCat" style="height: 100px" multiple data-selected-text-format="count > 1">
                                    <option>-- Choisir --</option>
                                </select>
                            </div>
                            <div class="col-xl-12 col-md-12">
                                <div class="section-headline margin-top-25 margin-bottom-12">
                                    <h5>Prix Min</h5>
                                </div>
                                <input type="number" id="prixMin" placeholder="Prix Minimal">
                            </div>
                            <div class="col-xl-12 col-md-12">
                                <div class="section-headline margin-top-25 margin-bottom-12">
                                    <h5>Prix Max</h5>
                                </div>
                                <input type="number" id="prixMax" placeholder="Prix Maximal">
                            </div>
                        </form>

                        <button onclick="ajouterCompetence()" class="button full-width button-sliding-icon ripple-effect"
                            form="login-form">Ajouter <i class="icon-material-outline-arrow-right-alt"></i></button>
                    </div>

                </div>
            </div>
        </div>
    @endsection

    @section('script')
        <script>
            $(document).ready(function() {
                //afficher les sous catégorie


                $('#titre').on('change', function() {
                    if ($("#titre").val() != "") {
                        $.ajax({
                            url: '/prestataires/chercher/souscompetence/' + $("#titre").val(),
                            type: 'GET',
                            success: function(response) {
                                $("#sousCat").html('');
                                if (response.array) {
                                    response.array.forEach(element => {
                                        $("#sousCat").append('<option value="' + element
                                            .titre_sous_categorie + '">' + element
                                            .titre_sous_categorie + ' </option>')
                                    });
                                }
                            },
                            error: function(xhr) {
                                alert("Impossible de récupérer ces données");
                            }
                        });
                    }

                });
            })

            function ajouterCompetence() {
                var user_id = $('#user_id').val()
                var categorie_id = $('#titre').val()
                var sous_cat = $('#sousCat').val()
                var prix_min = $('#prixMin').val()
                var prix_max = $('#prixMax').val()
                var notification = document.getElementById('notification')
                let array = []
                for (let index = 0; index < sous_cat.length; index++) {
                    const element = sous_cat[index];
                    var competence = prompt("Entrer votre niveau de compétence de 1 à 10 sur : " + sous_cat[index], "");
                    array.push(competence)
                }
                $.ajax({
                    url: "{{ route('prestataires.ajouter_competence') }}",
                    type: "POST",
                    contentType: 'application/x-www-form-urlencoded',
                    data: {
                        "_token": '{{ csrf_token() }}',
                        user_id: user_id,
                        categorie_id: categorie_id,
                        sous_cat: sous_cat,
                        prix_min: prix_min,
                        prix_max: prix_max,
                        competences: array
                    },
                    dataType: 'JSON',
                    success: function(response) {
                        if (response.warrning) { // when status code is 422, it's a validation issue

                            notification.innerHTML =
                                `<div class="notification warning closeable">
                            <p>${response.warrning}</p>
                            <a class="close" href="#"></a>
                        </div>`

                        } else if (response.success) {
                            notification.innerHTML =
                                `<div class="notification success closeable">
                            <p>${response.success}</p>
                            <a class="close" href="#"></a>
                        </div>`
                            console.log(response.array)
                            //$('.contenu').append(response.array)
                            window.location.reload();
                        }
                    },
                    error: function(err) {
                        notification.innerHTML = `
                        <div class="notification error closeable">
                            <p>La compétence choisi est incorrect !</p>
                            <a class="close" href="#"></a>
                        </div>
                        `
                    }
                });
            }

            function supprimerCompetence(categorie, prest, id) {
                var message = document.getElementById('message')

                url = "/prestataires/supprimer/competence/" + categorie + "/" + prest + "/" + id;

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(response) {
                        if (response) {
                            message.innerHTML =
                                `<div class="notification success closeable  mt-3">
                            <p>${response.success}</p>
                            <a class="close" href="#"></a>
                        </div>`
                            $("#categorie_" + categorie).remove();
                        }
                    }
                });
            }
        </script>
    @endsection
