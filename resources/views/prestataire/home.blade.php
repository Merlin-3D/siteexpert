@extends('layouts.app')

@section('title')
<i class="icon-material-outline-dashboard"></i> Prestataire -- {{ Auth::user()->name }}
@endsection

@section('subtitle')
Tableau de bord
@endsection

@section('menu')
Home
@endsection

@section('content')
<div class="fun-facts-container">
    <div class="fun-fact" data-fun-fact-color="#efa80f">
        <div class="fun-fact-text">
            <span>Vues</span>
            <h4> {{ $click_prestataire }}</h4>
        </div>
        <div class="fun-fact-icon"><i class="icon-feather-eye"></i></div>
    </div>
    <div class="fun-fact" data-fun-fact-color="#36bd78">
        <div class="fun-fact-text">
            <span>Compétences</span>
            <h4>{{ $competences }}</h4>
        </div>
        <div class="fun-fact-icon"><i class="icon-line-awesome-puzzle-piece"></i></div>
    </div>
    {{-- <div class="fun-fact" data-fun-fact-color="#b81b7f">
        <div class="fun-fact-text">
            <span>Réalisations</span>
            <h4>4</h4>
        </div>
        <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
    </div> --}}
    <!-- Last one has to be hidden below 1600px, sorry :( -->
    <div class="fun-fact" data-fun-fact-color="#2a41e6">
        <div class="fun-fact-text">
            <span>Entreprises</span>
            <h4>{{ $entreprises }}</h4>
        </div>
        <div class="fun-fact-icon"><i class="icon-material-outline-business"></i></div>
    </div>
</div>
@endsection
