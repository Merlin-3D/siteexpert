@extends('layouts.app')

@section('content2')
<div class="container">
	<div class="row">
		<div class="col-xl-5 offset-xl-3">


			<div class="login-register-page">
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Administration</h3>
					<span>Console d'administration</span>
				</div>

				<!-- Form -->
				<form method="POST" action="{{ route('login') }}">
                    @csrf
					<div class="input-with-icon-left">
						<i class="icon-material-baseline-mail-outline"></i>
						<input id="email" type="email" class="input-text with-border @error('email') is-invalid @enderror"name="email" value="{{ old('email') }}" required autocomplete="email"/>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

					<div class="input-with-icon-left">
						<i class="icon-material-outline-lock"></i>
						<input id="password" type="password" class="input-text with-border @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"/>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>


				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect margin-top-10" type="submit" >Connexion <i class="icon-material-outline-arrow-right-alt"></i></button>
                </form>
				<!-- Social Login -->

			</div>

		</div>
	</div>
</div>

@endsection
