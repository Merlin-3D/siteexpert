<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Expert</title>
        <link rel="stylesheet" href="{{ asset('css/media-query.css') }}">
        @yield('css')
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/colors/blue.css') }}">
        <link rel="stylesheet" href="{{ asset('css/partner.css') }}">
    </head>
    <body >
        <div id="wrapper">

            @include('client.layouts.header')

            @yield('content')

            @include('client.layouts.footer')

            @include('client.layouts.modal')
        </div>
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        @yield('script')

        <script src="{{ asset('js/jquery-migrate-3.0.0.min.js') }}"></script>
        <script src="{{ asset('js/mmenu.min.js') }}"></script>
        <script src="{{ asset('js/tippy.all.min.js') }}"></script>
        <script src="{{ asset('js/simplebar.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-slider.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
        <script src="{{ asset('js/snackbar.js') }}"></script>
        <script src="{{ asset('js/clipboard.min.js') }}"></script>
        <script src="{{ asset('js/counterup.min.js') }}"></script>
        <script src="{{ asset('js/magnific-popup.min.js') }}"></script>
        <script src="{{ asset('js/slick.min.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>

    </body>
</html>
