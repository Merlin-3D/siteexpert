<div class="col-xl-8 col-lg-8 content-right-offset">
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#about"> <i class="icon-material-outline-person-pin"></i> A Propos de moi</a></li>
            <li><a href="#services"><i class="icon-line-awesome-puzzle-piece"></i> Mes Catégories</a></li>
            <li><a href="#realisations"><i class="icon-line-awesome-suitcase
                "></i> Mes Réalisations</a></li>
            <li><a href="#client"><i class="icon-material-outline-business"></i> Clients</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- about -->
            <div class="popup-tab-content" id="about">
                <!-- Welcome Text -->
                <!-- Page Content -->
                <div class="single-page-section">
                    <h3 class="margin-bottom-25">À propos de moi</h3>
                    <p class="text-justify">
                        {{ $prestataires[0]->description }}
                    </p>
                </div>

                <!-- Boxed List -->
                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-thumb-up"></i> commentaires</h3>
                    </div>
                    <ul class="boxed-list-ul liste-comment">
                        @foreach ($commentaires as $commentaire)
                            <li id="comment_id{{ $commentaire->id }}">
                                <div class="boxed-list-item">
                                    <!-- Content -->
                                    <div class="item-content">
                                        <h4> {{ $commentaire->client->nom }}
                                            <span>{{ $commentaire->client->email }}</span>
                                        </h4>
                                        <div class="item-details margin-top-10">
                                            <div class="detail-item"><i class="icon-material-outline-date-range"></i>
                                                le. {{ date('d-m-Y', strtotime($commentaire->created_at)) }} </div>
                                        </div>
                                        <div class="item-description">
                                            <p class="text-justify">{{ $commentaire->commentaire }} </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach


                    </ul>

                    <!-- Pagination -->
                    <div class="clearfix"></div>
                    @if (Auth::user())
                        <div class="row">
                            <div class="col-xl-12">

                                <h3 class="margin-top-35 margin-bottom-30">Laisser un commentaire</h3>

                                <!-- Form -->
                                <form method="post" id="add-comment">
                                    @csrf
                                    <div class="row">

                                    </div>
                                    <input type="hidden" value={{ Auth::user()->id }} name="client_id">
                                    <input type="hidden" value={{ $prestataires[0]->id }} name="prestataire_id">
                                    <span class="text-danger" id="commentaire-error"></span>

                                    <textarea name="commentaire" cols="30" rows="5"
                                        placeholder="Commentaire"></textarea>


                                    <!-- Button -->
                                    <button class="button button-sliding-icon ripple-effect margin-bottom-40"
                                        type="submit">Poster le Commentaire <i
                                            class="icon-material-outline-arrow-right-alt"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    @endif

                    <!-- Pagination / End -->

                </div>
            </div>

            <!-- services -->
            <div class="popup-tab-content" id="services">
                <!-- Welcome Text -->
                <ul class="dashboard-box-list">
                    @foreach ($categories as $categorie)
                        @foreach ($categorie->prestataire as $prest)
                            @if ($prest->id == $prestataires[0]->id)
                                <li>
                                    <!-- Job Listing -->
                                    <div class="">

                                        <!-- Job Listing Details -->
                                        <div class="job-listing-details">

                                            <!-- Details -->
                                            <div class="job-listing-description">
                                                <h4 class="job-listing-title"><a href="#">{{ $categorie->titre }}</a>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="buttons-to-right always-visible margin-bottom-0">
   @foreach ($categorie->service as $service)
                                @if ( $prestataires[0]->id == $prest->pivot->prestataire_id && $categorie->id == $prest->pivot->categories_id)
                                <span
                                class="dashboard-status-button green" style="width: 200px">{{ $service->titreService }}
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: {{ $service->niveau }}0%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="10">{{ $service->niveau }}</div>
                                  </div>
                            </span>

                                @endif
                            @endforeach
                                    </div>

                                    {{-- <ul class="dashboard-task-info">

                                        @foreach ($categorie->prestataire as $tarif)

                                            <li>
                                                    <strong>{{ $prest->pivot->prix_min }}
                                                    XAF</strong><span>Tarif Min</span>
                                            </li>

                                        @endforeach
                                    </ul> --}}
                                </li>
                            @endif
                        @endforeach
                    @endforeach

                </ul>

            </div>

            <!-- realisations -->
            <div class="popup-tab-content" id="realisations">
                <!-- Welcome Text -->

                <!-- Job Listing Details -->
                @foreach ($categories as $categorie)
                    @foreach ($categorie->prestataire as $prest)
                        @if ($prest->id == $prestataires[0]->id)

                            @foreach ($categorie->realisation as $realisation)
                                <div class="job-listing-details">
                                    <!-- Details -->
                                    <div class="job-listing-description">
                                        <h3 class="job-listing-title"><a href="#">{{ $categorie->titre }}</a></h3>

                                        <!-- Job Listing Footer -->
                                        <div class="job-listing-footer">
                                            <ul>
                                                <li><i class="icon-material-outline-date-range"></i> Posté le
                                                    {{ $categorie->created_at }}</li>

                                            </ul>
                                            <div class="row">
                                                @foreach ($categorie->realisation as $realisation)
                                                    <div class="col-lg-2 col-md-4 col-6">
                                                        <a href="#"><img
                                                                src="{{ asset('/realisations/' . $realisation->url) }}"
                                                                id="myImg"
                                                                alt="Bluehost logo"></a>
                                                    </div>
                                                       <p class="text-justify">{{ $realisation->description }}</p>

                                                @endforeach


                                            </div>
                                        </div>

                                    </div>

                                </div>
                            @endforeach
                        @endif
                    @endforeach
                @endforeach


            </div>

            <!-- client -->
            <div class="popup-tab-content" id="client">
                <!-- Welcome Text -->
                <div class="col-xl-12">
                    <div class="dashboard-box margin-top-0">


                        <div class="content">
                            <ul class="dashboard-box-list">
                                @foreach ($prestataires[0]->entreprise as $entreprise)
                                    <li>
                                        <!-- Job Listing -->
                                        <div class="job-listing">

                                            <!-- Job Listing Details -->
                                            <div class="job-listing-details">

                                                <!-- Logo -->
                                                <a href="#" class="job-listing-company-logo">
                                                    <img src="{{ asset('images/company-logo-02.png') }}" style=" border-radius: 50%;" alt="">
                                                </a>

                                                <!-- Details -->
                                                <div class="job-listing-description">
                                                    <h3 class="job-listing-title"><a
                                                            href="#">{{ $entreprise->tache }}</a></h3>

                                                    <!-- Job Listing Footer -->
                                                    <div class="job-listing-footer">
                                                        <ul>
                                                            <li><i class="icon-material-outline-business"></i>
                                                                {{ $entreprise->nom }}</li>

                                                            <li><i class="icon-material-outline-access-time"></i> le :
                                                                {{ $entreprise->date_publication }}</li>
                                                                @if ($entreprise->contact)
                                                                 <li><i class="icon-feather-phone"></i>
                                                                    {{ $entreprise->contact }}</li>
                                                                @endif

                                                        </ul>
                                                        <h3 class="job-listing-title"><a href="#"> {{ $entreprise->temoignage }} </a></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </li>

                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!-- Boxed List / End -->


    <!-- Boxed List / End -->

</div>
<div id="small-dialog" class="zoom-anim-dialog mfp-hide ">
    <!--Tabs -->
    <div class="sign-in-form">

        {{-- <div class="popup-tabs-nav">
            <li><a href="#tab">Mes Contacts</a></li>
        </div> --}}

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Comment Joindre {{ $prestataires[0]->nom }} ?</h3>

                    <!-- Form -->
                    <form method="post" id="leave-company-review-form">
                        <div class="clearfix"></div>
                        <hr>
                        <h3 style="font-size: 18px" onclick="count_click({{ $prestataires[0]->id }})"><i class="icon-feather-phone"></i> Télèphone : <strong><a href="tel:{{ $prestataires[0]->telephone }}">(+237){{ $prestataires[0]->telephone }}</a></strong> </h3>
                        <h3 style="font-size: 18px"><i class="icon-line-awesome-whatsapp"></i> Whatsapp : <strong> <a href="https://api.whatsapp.com/send?phone=+237{{ $prestataires[0]->telephone }}&text=Hi%20we%20need%20help%20regarding%20something">{{ $prestataires[0]->telephone }}</a></strong> </h3>
                        <h3 style="font-size: 18px"><i class="icon-material-outline-email"></i> E-mail : <strong><a href="mailto:{{ $prestataires[0]->email }}">{{ $prestataires[0]->email }}</a> </strong> </h3>
                </div>

                </form>

            </div>
        </div>
    </div>
</div>
