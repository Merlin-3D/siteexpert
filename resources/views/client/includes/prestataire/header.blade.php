{{-- <div class="single-page-header freelancer-header"  data-background-image="{{ $prestataires[0]->banniere ? asset('/bannieres/' . $prestataires[0]->banniere) : asset('images/banner/pexels-canva-studio-3194519.jpg') }}">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						<div class="header-image freelancer-avatar"><img src="{{ $prestataires[0]->file ? asset('/avatars/' . $prestataires[0]->file) : asset('images/company-logo-05.png') }}" alt=""></div>
						<div class="header-details">
							<h3>{{ $prestataires[0]->nom }} <span>{{ $prestataires[0]->email }}</span></h3>
							<ul> --}}
								{{-- <li><div class="star-rating" data-rating="5.0"></div></li> --}}
								{{-- <li>{{ $prestataires[0]->telephone }}</li> --}}

								{{-- <li><div class="verified-badge-with-title">Verified</div></li> --}}
							{{-- </ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> --}}

<div class="container mb-2">
    <div class="row">
        <div class="col-md-12">
            <div class="fb-profile-block">
                <div class="fb-profile-block-thumb cover-container"></div>
                <div class="profile-img">
                    <a href="#">
                        <img src="{{ $prestataires[0]->file ? asset('/avatars/' . $prestataires[0]->file) : asset('images/company-logo-05.png') }}" style="border-radius: 50%;" alt="">
                    </a>
                </div>
                <div class="profile-name">
                    <h2>{{ $prestataires[0]->nom }}</h2>
                    <h3 class="text-white">{{ $prestataires[0]->email }}</h3>
                </div>

            </div>
        </div>
    </div>
</div>
