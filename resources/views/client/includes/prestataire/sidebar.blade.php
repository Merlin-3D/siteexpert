<div class="col-xl-4 col-lg-4">
    <div class="sidebar-container">

        <!-- Profile Overview -->
        <div class="profile-overview text-right">
            {{-- <div class="overview-item"><strong>{{ $prestataires[0]->taux_horaire }} XAF</strong><span>Taux horaire</span></div> --}}
            <div class="overview-item"><strong>{{ $count_entreprise }}</strong><span>Travaux </span></div>
        </div>

        <!-- Button -->
        <a href="#small-dialog" class="apply-now-button popup-with-zoom-anim margin-bottom-50">Affichez Mes Contacts <i
                class="icon-material-outline-arrow-right-alt"></i></a>



        <!-- Widget -->
        <div class="sidebar-widget text-right">
            <div class="freelancer-socials margin-top-25">
                <ul>
                    <li><i class="icon-material-outline-location-on"></i> {{ $prestataires[0]->ville }}</li>
                    <li><i class="icon-feather-eye ml-2"></i> {{ $prestataires[0]->click }}</li>

                </ul>
            </div>
        </div>

        <!-- Widget -->
        {{-- <div class="sidebar-widget">
            <h3>Compétences</h3>
            <div class="task-tags">
                @foreach ($categories as $categorie)
                    @foreach ($categorie->prestataire as $prest)
                        @if ($prest->id == $prestataires[0]->id)
                            @foreach ($categorie->service as $service)
                                @if ( $prestataires[0]->id == $prest->pivot->prestataire_id && $categorie->id == $prest->pivot->categories_id)

                                    <span>{{ $service->titreService }}</span>

                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endforeach

            </div>
        </div> --}}



    </div>
</div>
