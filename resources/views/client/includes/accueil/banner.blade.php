<div class="intro-banner" data-background-image="{{ asset('images/banner/pexels-canva-studio-3194519.jpg') }}">

	<!-- Transparent Header Spacer -->
	<div class="transparent-header-spacer"></div>

	<div class="container">

		<!-- Intro Headline -->
		<div class="row">
			<div class="col-md-12">
				<div class="banner-headline">
					<h3>
						<strong>Des experts certifiés à votre service.</strong>
						<br>
						<span>Venez découvrir de milliers de profils <span style="color: #2a41e8; font-size: 1.1em"> <b>d'experts</b> </span></span>
						<span>de divers domaine pour vous procurer des services de qualité.</span>

					</h3>
				</div>
			</div>
		</div>
        <form action="{{ route('client.rechercher_accueil') }}" method="POST">
            @csrf
		<!-- Search Bar -->
		<div class="row">
			<div class="col-md-12">
				<div class="intro-banner-search-form margin-top-95">
					<!-- Search Field -->
					<div class="intro-search-field with-autocomplete">
						<label for="autocomplete-input" class="field-title ripple-effect">Ou?</label>
						<div class="input-with-icon">
							<input  type="text" placeholder="Ville" id="ville" name="ville">
							<i class="icon-material-outline-location-on"></i>
						</div>
					</div>

					<!-- Search Field -->
					<div class="intro-search-field">
						<label for ="intro-keywords" class="field-title ripple-effect">De quoi avez-vous besoin?</label>
						<input id="intro-keywords" type="text" placeholder="exp flutter" name="service"  id="service">
					</div>

					<!-- Search Field -->
					<div class="intro-search-field">
						<select class="selectpicker default" title="Toutes les Catégories" name="id_categorie"  id="id_categorie">
                            @foreach ($categories as $categorie)
                               <option value="{{ $categorie->id }}">{{ $categorie->titre }}</option>
                            @endforeach


						</select>
					</div>

					<!-- Button -->
					<div class="intro-search-button">
						<button class="button ripple-effect" type="submit" >Rechercher</button>
					</div>
                </form>
				</div>
			</div>
		</div>
    </form>
		<!-- Stats -->
		<div class="row">
			<div class="col-md-12">
				<ul class="intro-stats margin-top-45 hide-under-992px">
					<li>
						<strong class="counter">1,586</strong>
						<span>Jobs En Ligne</span>
					</li>

				</ul>
			</div>
		</div>

	</div>
</div>
