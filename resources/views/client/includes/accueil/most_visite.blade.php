<div class="section gray padding-top-65  padding-bottom-55 full-width-carousel-fix">
	<div class="container">
		<div class="row">

			<div class="col-xl-12">
				<!-- Section Headline -->
				<div class="section-headline margin-top-0 margin-bottom-25">
					<h3>Les freelancer les mieux notés</h3>
					<a href="freelancers-grid-layout.html" class="button headline-link">Voir tous les profils</a>
				</div>
			</div>

			<div class="col-xl-12">
				<div class="default-slick-carousel freelancers-container freelancers-grid-layout">

                    <div class="freelancer">

						<!-- Overview -->
						<div class="freelancer-overview">
							<div class="freelancer-overview-inner">

								<!-- Bookmark Icon -->

								<!-- Avatar -->
								<div class="freelancer-avatar">
									<a href="single-freelancer-profile.html"><img src="{{asset('images/user-avatar-placeholder.png')}}" alt=""></a>
								</div>

								<!-- Name -->
								<div class="freelancer-name">
									<h4><a href="single-freelancer-profile.html">Douglas Wafo</h4>
									<span>UI/UX Designer</span>
								</div>

								<!-- Rating -->
								<div class="freelancer-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>

						<!-- Details -->
						<div class="freelancer-details">
							<div class="freelancer-details-list text-center ">
								<ul>
									<li class="text-center">Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
									<li class="text-center">Prix <strong>60XAF</strong></li>
									<li class="text-center">vues <strong>95</strong></li>
								</ul>
							</div>
							<a href="{{ route('page_prestataire', 1) }}" class="button button-sliding-icon ripple-effect">Voir le Profil <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
                    <div class="freelancer">

						<!-- Overview -->
						<div class="freelancer-overview">
							<div class="freelancer-overview-inner">

								<!-- Bookmark Icon -->

								<!-- Avatar -->
								<div class="freelancer-avatar">
									<a href="single-freelancer-profile.html"><img src="{{asset('images/user-avatar-placeholder.png')}}" alt=""></a>
								</div>

								<!-- Name -->
								<div class="freelancer-name">
									<h4><a href="single-freelancer-profile.html">Douglas Wafo</h4>
									<span>UI/UX Designer</span>
								</div>

								<!-- Rating -->
								<div class="freelancer-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>

						<!-- Details -->
						<div class="freelancer-details">
							<div class="freelancer-details-list text-center">
								<ul>
									<li class="text-center">Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
									<li class="text-center">Prix <strong>60XAF</strong></li>
									<li class="text-center">vues <strong>95</strong></li>
								</ul>
							</div>
							<a href="{{ route('page_prestataire', 1) }}" class="button button-sliding-icon ripple-effect">Voir le Profil <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
                    <div class="freelancer">

						<!-- Overview -->
						<div class="freelancer-overview">
							<div class="freelancer-overview-inner">

								<!-- Bookmark Icon -->

								<!-- Avatar -->
								<div class="freelancer-avatar">
									<a href="single-freelancer-profile.html"><img src="{{asset('images/user-avatar-placeholder.png')}}" alt=""></a>
								</div>

								<!-- Name -->
								<div class="freelancer-name">
									<h4><a href="single-freelancer-profile.html">Douglas Wafo</h4>
									<span>UI/UX Designer</span>
								</div>

								<!-- Rating -->
								<div class="freelancer-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>

						<!-- Details -->
						<div class="freelancer-details">
							<div class="freelancer-details-list text-center">
								<ul>
									<li class="text-center">Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
									<li class="text-center">Prix <strong>60XAF</strong></li>
									<li class="text-center">vues <strong>95</strong></li>
								</ul>
							</div>
							<a href="{{ route('page_prestataire', 1) }}" class="button button-sliding-icon ripple-effect">Voir le Profil <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
                    <div class="freelancer">

						<!-- Overview -->
						<div class="freelancer-overview">
							<div class="freelancer-overview-inner">

								<!-- Bookmark Icon -->

								<!-- Avatar -->
								<div class="freelancer-avatar">
									<a href="single-freelancer-profile.html"><img src="{{asset('images/user-avatar-placeholder.png')}}" alt=""></a>
								</div>

								<!-- Name -->
								<div class="freelancer-name">
									<h4><a href="single-freelancer-profile.html">Douglas Wafo</h4>
									<span>UI/UX Designer</span>
								</div>

								<!-- Rating -->
								<div class="freelancer-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>

						<!-- Details -->
						<div class="freelancer-details">
							<div class="freelancer-details-list text-center">
								<ul>
									<li class="text-center">Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
									<li class="text-center">Prix <strong>60XAF</strong></li>
									<li class="text-center">vues <strong>95</strong></li>
								</ul>
							</div>
							<a href="{{ route('page_prestataire', 1) }}" class="button button-sliding-icon ripple-effect">Voir le Profil <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
                    <div class="freelancer">

						<!-- Overview -->
						<div class="freelancer-overview">
							<div class="freelancer-overview-inner">

								<div class="freelancer-avatar">
									<a href="single-freelancer-profile.html"><img src="{{asset('images/user-avatar-placeholder.png')}}" alt=""></a>
								</div>

								<!-- Name -->
								<div class="freelancer-name">
									<h4><a href="single-freelancer-profile.html">Douglas Wafo</h4>
									<span>UI/UX Designer</span>
								</div>

								<!-- Rating -->
								<div class="freelancer-rating">
									<div class="star-rating" data-rating="5.0"></div>
								</div>
							</div>
						</div>

						<!-- Details -->
						<div class="freelancer-details">
							<div class="freelancer-details-list text-center">
								<ul>
									<li class="text-center">Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
									<li class="text-center">Prix <strong>60XAF</strong></li>
									<li class="text-center">vues <strong>95</strong></li>
								</ul>
							</div>
							<a href="{{ route('page_prestataire', 1) }}" class="button button-sliding-icon ripple-effect">Voir le Profil <i class="icon-material-outline-arrow-right-alt"></i></a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
