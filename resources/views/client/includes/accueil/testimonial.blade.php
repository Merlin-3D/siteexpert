<div class="section gray padding-top-65 padding-bottom-55">

	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<!-- Section Headline -->
				<div class="section-headline centered margin-top-0 margin-bottom-5">
					<h3>Témoignages</h3>
				</div>
			</div>
		</div>
	</div>

	<!-- Categories Carousel -->
	<div class="fullwidth-carousel-container margin-top-20">
		<div class="testimonial-carousel testimonials">

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="{{asset('images/user-avatar-placeholder.png')}}" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Douglas Wafo</h4>
						 <span>Freelancer</span>
					</div>
					<div class="testimonial">
                        Libérez efficacement des informations cross-média sans valeur cross-média. Maximisez rapidement les livrables opportuns pour les schémas en temps réel. Maintenez considérablement les solutions click-and-mortar sans solutions fonctionnelles.</div>
				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="{{asset('images/user-avatar-placeholder.png')}}" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Douglas Wafo</h4>
						 <span>Freelancer</span>
					</div>
					<div class="testimonial">
                        Libérez efficacement des informations cross-média sans valeur cross-média. Maximisez rapidement les livrables opportuns pour les schémas en temps réel. Maintenez considérablement les solutions click-and-mortar sans solutions fonctionnelles.</div>
				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="{{asset('images/user-avatar-placeholder.png')}}" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Douglas Wafo</h4>
						 <span>Client</span>
					</div>
					<div class="testimonial">
                        Libérez efficacement des informations cross-média sans valeur cross-média. Maximisez rapidement les livrables opportuns pour les schémas en temps réel. Maintenez considérablement les solutions click-and-mortar sans solutions fonctionnelles.</div>
				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="{{asset('images/user-avatar-placeholder.png')}}" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Douglas Wafo</h4>
						 <span>Freelancer</span>
					</div>
					<div class="testimonial">
                        Libérez efficacement des informations cross-média sans valeur cross-média. Maximisez rapidement les livrables opportuns pour les schémas en temps réel. Maintenez considérablement les solutions click-and-mortar sans solutions fonctionnelles.</div>
				</div>
			</div>

			<!-- Item -->
			<div class="fw-carousel-review">
				<div class="testimonial-box">
					<div class="testimonial-avatar">
						<img src="{{asset('images/user-avatar-placeholder.png')}}" alt="">
					</div>
					<div class="testimonial-author">
						<h4>Douglas Wafo</h4>
						 <span>Client</span>
					</div>
					<div class="testimonial">
                        Libérez efficacement des informations cross-média sans valeur cross-média. Maximisez rapidement les livrables opportuns pour les schémas en temps réel. Maintenez considérablement les solutions click-and-mortar sans solutions fonctionnelles.</div>
				</div>
			</div>

		</div>
	</div>
	<!-- Categories Carousel / End -->

</div>
