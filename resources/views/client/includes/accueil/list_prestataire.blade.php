<div class="section   padding-top-65 padding-bottom-75">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">

                <!-- Section Headline -->


                <!-- Job Listing -->
                @if (empty($vedettes))
                <div class="section-headline margin-top-0 margin-bottom-35">
                    <h3>Resutats de recherche
                        <strong>
                            @if (empty($results))
                            {{ $ville }}
                            @else
                            {{ $ville }}
                            @endif
                            </strong> </h3>
                </div>
                <div class="listings-container compact-list-layout margin-top-35">


                    {{-- @foreach (App\Models\Prestataire::where('ville', $ville)->get() as $recherche) --}}
                    @if (empty($results))

                    @else
                    @foreach ($results as $result)
                    <a href="{{ route('page_prestataire', $result->id) }}"
                        class="job-listing with-apply-button">

                        <!-- Job Listing Details -->
                        <div class="job-listing-details">

                            <!-- Logo -->
                            <div class="job-listing-company-logo">
                                <img
                                src="{{ $result->file ? asset('/avatars/' . $result->file) : asset('images/company-logo-05.png') }}"
                                alt="">
                            </div>

                            <!-- Details -->
                            <div class="job-listing-description">
                                <h3 class="job-listing-title">{{ $result->nom }} {{ $result->prenom }}
                                </h3>

                                <!-- Job Listing Footer -->
                                <div class="job-listing-footer">
                                    <ul>
                                        <li><i class="icon-material-outline-location-on"></i>
                                            {{ $result->ville }}</li>
                                        <li><i class="icon-feather-eye"></i> {{ $result->click }}</li>
                                        <li><i class="icon-material-outline-business-center"></i> Réalisation
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Apply Button -->
                            <span class="list-apply-button ripple-effect">Voir le profil</span>
                        </div>
                    </a>
                    @endforeach
                    @endif

                </div>

                @else
                    <div class="section-headline margin-top-0 margin-bottom-35">
                        <h3>Les Experts en vedette</h3>
                    </div>

                    <!-- Jobs Container -->
                    <div class="listings-container compact-list-layout margin-top-35">

                        @foreach ($vedettes as $vedette)

                            <a href="{{ route('page_prestataire', $vedette->id) }}"
                                class="job-listing with-apply-button">

                                <!-- Job Listing Details -->
                                <div class="job-listing-details">

                                    <!-- Logo -->
                                    <div class="job-listing-company-logo">
                                        <img src="{{ $vedette->file ? asset('/avatars/' . $vedette->file) : asset('images/company-logo-05.png') }}" alt="">
                                    </div>

                                    <!-- Details -->
                                    <div class="job-listing-description">
                                        <h3 class="job-listing-title">{{ $vedette->nom }} {{ $vedette->prenom }}
                                        </h3>

                                        <!-- Job Listing Footer -->
                                        <div class="job-listing-footer">
                                            <ul>
                                                <li><i class="icon-material-outline-location-on"></i>
                                                    {{ $vedette->ville }}</li>
                                                <li><i class="icon-feather-eye"></i> {{ $vedette->click }}</li>
                                                <li><i class="icon-material-outline-business-center"></i> Réalisation
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Apply Button -->
                                    <span class="list-apply-button ripple-effect">Voir le profil</span>
                                </div>
                            </a>

                        @endforeach
                    </div>
                @endif
                <!-- Jobs Container / End -->

            </div>
        </div>
        <div class="text-center">
            <a href="{{ route('page_categorie', '0') }}" class="button mt-3">Charger Plus</a>
        </div>
    </div>
</div>
