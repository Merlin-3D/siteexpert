<div class="section padding-top-60  padding-bottom-55">
    <div class="container">
        <div class="text-center">
            <span class="button">Les catégories les plus populaires</span>
        </div>
        <div class=" container row mt-5">
            <div class="col-12 col-md-8">
                <div class="col-12 col-sm-10 ml-3">
                    <p>Les Experts c’est jusqu’à lors plus de <strong>28</strong> égories couvertes, pour vous assurer
                        de
                        trouver votre profil </p>
                </div>
                <div class="box">
                    <div class="text-center">
                        <img src="{{ asset('images/images/design graphique.png') }}" class="image-cat" width="100px" alt="">
                        <hr class="image-cat" style="height:5px;border:none;color:#333;background-color:#333; width:80px">
                        <strong class="text-cat">Design Graphique</strong>
                    </div>
                    <div class="text-center"> <img src="{{ asset('images/images/digital marketing.png') }}" class="image-cat" width="100px" alt="">
                        <hr class="image-cat" style="height:5px;border:none;color:#333;background-color:#333; width:80px">
                        <strong class="text-cat">Marketing Digital</strong></div>
                    <div class="text-center"><img src="{{ asset('images/images/developpement.png') }}" class="image-cat" width="100px" alt="">
                        <hr class="image-cat" style="height:5px;border:none;color:#333;background-color:#333; width:80px">
                        <strong class="text-cat">Developpement Web</strong>
                    </div>

                </div>
            </div>
            <div class="col-6 col-md-4 elements">
                <div class="freelancer-avatar text-center">
                    <a href="single-freelancer-profile.html">
                        <img src="{{ asset('images/images/team.pn') }}g" height="200px"
                            alt=""></a>
                </div>
                <div class="freelancer-name text-center pt-3">
                    <p style=" margin:10px; color:#2a41e8; font-size: 4em; font-weight: 20px">+ 150</p>

                    <p class="pt-4"> <strong style="color: black; font-size: 2em">Profils d'experts</strong></p>
                </div>
            </div>
        </div>
    </div>

</div>
