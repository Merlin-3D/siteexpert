<div class="section border-top padding-top-45 padding-bottom-45">
	<!-- Logo Carousel -->
	<div class="container">
        <section class="logo-list">
            <div class="container">

                <div class="row logo-carousel">
                    <div class="col-lg-2 col-md-4 col-6">
                        <a href="#"><img src="https://www.markuptag.com/images/bluehost-logo.png" class="img-fluid" alt="Bluehost logo"></a>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <a href="#"><img src="https://www.markuptag.com/images/hostgator-logo.png" class="img-fluid" alt="Hostgator logo"></a>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <a href="#"><img src="https://www.markuptag.com/images/greengeeks-logo.png" class="img-fluid" alt="Green Geeks logo"></a>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <a href="#"><img src="https://www.markuptag.com/images/wordpress-logo.png" class="img-fluid" alt="WordPress logo"></a>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <a href="#"><img src="https://www.markuptag.com/images/dreamhost-logo.png" class="img-fluid" alt="DreamHost logo"></a>
                    </div>
                    <div class="col-lg-2 col-md-4 col-6">
                        <a href="#"><img src="https://www.markuptag.com/images/hostinger-logo.png" class="img-fluid" alt="Hostinger logo"></a>
                    </div>
                </div>
            </div>
        </section>
	</div>
</div>
