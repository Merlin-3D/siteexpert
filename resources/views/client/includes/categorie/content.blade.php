<div class="full-page-content-container" data-simplebar>
    <div class="full-page-content-inner">

        <h3 class="page-title">Resultats de recherche</h3>



        <div class="listings-container grid-layout margin-top-35">
            @if (empty($prestataires))
            <h3 class="page-title"><strong>{{ $reponse }}</strong></h3>
            @else
            @foreach ($prestataires as $prestataire)
            <a href="{{ route('page_prestataire', $prestataire->id) }}" class="job-listing">

          <!-- Job Listing Details -->
          <div class="job-listing-details">
              <!-- Logo -->
              <div class="job-listing-company-logo">
                  <img src="{{ asset('images/company-logo-01.png') }}" alt="">
              </div>

              <!-- Details -->
              <div class="job-listing-description">
                  <h4 class="job-listing-company">{{ $prestataire->nom }} </h4>
                  <h3 class="job-listing-title">{{ $prestataire->email }}</h3>
              </div>
          </div>

          <!-- Job Listing Footer -->
          <div class="job-listing-footer">
              <ul>
                  <li><i class="icon-material-outline-location-on"></i> {{ $prestataire->ville }}</li>
                  <li><i class="icon-feather-eye"></i> {{ $prestataire->click }}</li>
                  {{-- <li><i class="icon-material-outline-account-balance-wallet"></i> {{ $prestataires[0]->taux_horaire }} XAF</li> --}}
              </ul>
          </div>
      </a>
      @endforeach
            @endif

            <!-- Job Listing -->


        </div>

        <!-- Pagination -->
        <div class="clearfix"></div>
        <div class="pagination-container margin-top-20 margin-bottom-20">
            {{-- {{$prestataires->links()}}  --}}
        </div>
        <div class="clearfix"></div>
        <!-- Pagination / End -->


        <!-- Footer / End -->

    </div>
</div>
