<div class="full-page-sidebar">
    <form action="{{ route('client.rechercher_prestataire') }}" method="POST">
        @csrf
        <div class="full-page-sidebar-inner" data-simplebar>
            <div class="sidebar-container">

                <!-- Location -->
                <div class="sidebar-widget">
                    <h3>Localisation</h3>
                    <div class="input-with-icon">
                        <div id="autocomplete-container">
                            <select class="selectpicker" name="ville">
                                <option value="yaoundé">Yaoundé</option>
                                <option value="douala">Douala</option>
                                <option value="maroua">Maroua</option>
                            </select>
                        </div>
                        <i class="icon-material-outline-location-on"></i>
                    </div>
                </div>
                <!-- Category -->
                <div class="sidebar-widget">
                    <h3>Catégories</h3>
                    <select class="selectpicker default" id="titre" title="Toutes les catégories" name="categories_id">
                        @foreach ($categories as $categorie)
                            <option value="{{ $categorie->id }}"> {{ $categorie->titre }} </option>
                        @endforeach
                    </select>
                </div>

                <!-- Keywords -->
                <div class="sidebar-widget">
                    <h3>Mots clés</h3>
                    <select id="sousCat" name="service">
                        <option> -- Choisir -- </option>
                    </select>
                </div>

                {{-- <div class="keywords-container">
                    <div class="keyword-input-container">
                        <input type="text" class="keyword-input" placeholder="exemp. titre job" name="service" /> --}}
                        {{-- <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button> --}}
                    {{-- </div>
                    <div class="keywords-list">
                    </div>
                    <div class="clearfix"></div>
                </div> --}}


                <!-- Salary -->
                {{-- <div class="sidebar-widget">
					<h3>Prix</h3>
					<div class="margin-top-55"></div>

					<!-- Range Slider -->
					<input class="range-slider" name="prix" type="text" value="" data-slider-currency="XAF" data-slider-min="1000" data-slider-max="1500000" data-slider-step="100" data-slider-value="[1000,500000]"/>
				</div> --}}

                <!-- Tags -->
                <div class="sidebar-widget">
                    <h3>Tags</h3>

                    <div class="tags-container">
                        @foreach ($categories as $categorie)
                            @foreach ($categorie->souscategorie as $souscategorie)
                                <div class="tag">
                                    <input type="checkbox" id="tag{{ $souscategorie->id }}" name="tag" />
                                    <label
                                        for="tag{{ $souscategorie->id }}">{{ $souscategorie->titre_sous_categorie }}</label>
                                </div>
                            @endforeach
                        @endforeach


                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
            <!-- Sidebar Container / End -->

            <!-- Search Button -->
            <div class="sidebar-search-button-container">
                <button class="button ripple-effect" type="submit">Rechercher</button>
            </div>
            <!-- Search Button / End-->

        </div>
    </form>
</div>
