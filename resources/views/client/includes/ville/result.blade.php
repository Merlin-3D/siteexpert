<div class="col-xl-9 col-lg-8 content-left-offset">

    <h3 class="page-title">Resultats Yaoundé</h3>



    <!-- Freelancers List Container -->
    <div class="freelancers-container freelancers-list-layout compact-list margin-top-35">

        <!--Freelancer -->
        <div class="freelancer">

            <!-- Overview -->
            <div class="freelancer-overview">
                <div class="freelancer-overview-inner">

                    <!-- Bookmark Icon -->
                    <span class="bookmark-icon"></span>

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                        <a href="single-freelancer-profile.html"><img src="{{ asset('images/user-avatar-big-01.jpg') }}" alt=""></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                        <h4><a href="#">Douglas Wafo <img class="flag" src="images/flags/gb.svg" alt="" title="United Kingdom" data-tippy-placement="top"></a></h4>
                        <span>UI/UX Designer</span>
                        <!-- Rating -->
                        <div class="freelancer-rating">
                            <div class="star-rating" data-rating="4.9"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Details -->
            <div class="freelancer-details">
                <div class="freelancer-details-list">
                    <ul>
                        <li>Vue <strong><i class="icon-feather-eye"></i> 250</strong></li>
                        <li>Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
                        <li>Prix <strong>50000 XAF</strong></li>
                    </ul>
                </div>
                <a href="#" class="button button-sliding-icon ripple-effect">Voir le profil  <i class="icon-material-outline-arrow-right-alt"></i></a>
            </div>
        </div>
        <!-- Freelancer / End -->

        <!--Freelancer -->
        <div class="freelancer">

            <!-- Overview -->
            <div class="freelancer-overview">
                <div class="freelancer-overview-inner">

                    <!-- Bookmark Icon -->
                    <span class="bookmark-icon"></span>

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                        <a href="single-freelancer-profile.html"><img src="{{ asset('images/user-avatar-big-01.jpg') }}" alt=""></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                        <h4><a href="#">Douglas Wafo <img class="flag" src="images/flags/de.svg" alt="" title="Germany" data-tippy-placement="top"></a></h4>
                        <span>iOS Expert + Node Dev</span>
                        <!-- Rating -->
                        <div class="freelancer-rating">
                            <div class="star-rating" data-rating="4.2"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Details -->
            <div class="freelancer-details">
                <div class="freelancer-details-list">
                    <ul>
                        <li>Vue <strong><i class="icon-feather-eye"></i> 250</strong></li>
                        <li>Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
                        <li>Prix <strong>50000 XAF</strong></li>
                    </ul>
                </div>
                <a href="#" class="button button-sliding-icon ripple-effect">Voir le profil  <i class="icon-material-outline-arrow-right-alt"></i></a>
            </div>
        </div>
        <!-- Freelancer / End -->

        <!--Freelancer -->
        <div class="freelancer">

            <!-- Overview -->
            <div class="freelancer-overview">
                <div class="freelancer-overview-inner">
                    <!-- Bookmark Icon -->
                    <span class="bookmark-icon"></span>

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                        <a href="single-freelancer-profile.html"><img src="{{ asset('images/user-avatar-big-01.jpg') }}" alt=""></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                        <h4><a href="#">Douglas Wafo <img class="flag" src="images/flags/pl.svg" alt="" title="Poland" data-tippy-placement="top"></a></h4>
                        <span>Front-End Developer</span>
                        <!-- Rating -->
                    </div>
                </div>
            </div>

            <!-- Details -->
            <div class="freelancer-details">
                <div class="freelancer-details-list">
                    <ul>
                        <li>Vue <strong><i class="icon-feather-eye"></i> 250</strong></li>
                        <li>Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
                        <li>Prix <strong>50000 XAF</strong></li>
                    </ul>
                </div>
                <a href="#" class="button button-sliding-icon ripple-effect">Voir le profil  <i class="icon-material-outline-arrow-right-alt"></i></a>
            </div>
        </div>
        <!-- Freelancer / End -->

        <!--Freelancer -->
        <div class="freelancer">

            <!-- Overview -->
            <div class="freelancer-overview">
                    <div class="freelancer-overview-inner">
                    <!-- Bookmark Icon -->
                    <span class="bookmark-icon"></span>

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                        <a href="single-freelancer-profile.html"><img src="{{ asset('images/user-avatar-big-01.jpg') }}" alt=""></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                        <h4><a href="#">Douglas Wafo <img class="flag" src="images/flags/au.svg" alt="" title="Australia" data-tippy-placement="top"></a></h4>
                        <span>Magento Certified Developer</span>
                        <!-- Rating -->
                        <div class="freelancer-rating">
                            <div class="star-rating" data-rating="5.0"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Details -->
            <div class="freelancer-details">
                <div class="freelancer-details-list">
                    <ul>
                        <li>Vue <strong><i class="icon-feather-eye"></i> 250</strong></li>
                        <li>Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
                        <li>Prix <strong>50000 XAF</strong></li>
                       </ul>
                </div>
                <a href="#" class="button button-sliding-icon ripple-effect">Voir le profil  <i class="icon-material-outline-arrow-right-alt"></i></a>
            </div>
        </div>
        <!-- Freelancer / End -->

        <!--Freelancer -->
        <div class="freelancer">

            <!-- Overview -->
            <div class="freelancer-overview">
                    <div class="freelancer-overview-inner">
                    <!-- Bookmark Icon -->
                    <span class="bookmark-icon"></span>

                    <!-- Avatar -->
                    <div class="freelancer-avatar">
                        <a href="single-freelancer-profile.html"><img src="{{ asset('images/user-avatar-big-01.jpg') }}" alt=""></a>
                    </div>

                    <!-- Name -->
                    <div class="freelancer-name">
                        <h4><a href="#">Douglas Wafo <img class="flag" src="images/flags/it.svg" alt="" title="Italy" data-tippy-placement="top"></a></h4>
                        <span>Laravel Dev</span>
                        <!-- Rating -->
                        <div class="freelancer-rating">
                            <div class="star-rating" data-rating="4.5"></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Details -->
            <div class="freelancer-details">
                <div class="freelancer-details-list">
                    <ul>
                        <li>Vue <strong><i class="icon-feather-eye"></i> 250</strong></li>
                        <li>Localisation <strong><i class="icon-material-outline-location-on"></i> Yaoundé</strong></li>
                        <li>Prix <strong>50000 XAF</strong></li>
                    </ul>
                </div>
                <a href="#" class="button button-sliding-icon ripple-effect">Voir le profil  <i class="icon-material-outline-arrow-right-alt"></i></a>
            </div>
        </div>
        <!-- Freelancer / End -->


    </div>
    <!-- Freelancers Container / End -->


    <!-- Pagination -->
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-40 margin-bottom-60">
                <nav class="pagination">
                    <ul>
                        <li class="pagination-arrow"><a href="#" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-left"></i></a></li>
                        <li><a href="#" class="ripple-effect">1</a></li>
                        <li><a href="#" class="current-page ripple-effect">2</a></li>
                        <li><a href="#" class="ripple-effect">3</a></li>
                        <li><a href="#" class="ripple-effect">4</a></li>
                        <li class="pagination-arrow"><a href="#" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- Pagination / End -->

</div>
