<div class="col-xl-3 col-lg-4">
    <div class="sidebar-container">



        <!-- Category -->
        <div class="sidebar-widget">
            <h3>Catégories</h3>
            <select class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="Toutes les Catégories" >
                <option>Admin Support</option>
                <option>Customer Service</option>
                <option>Data Analytics</option>
                <option>Design & Creative</option>
                <option>Legal</option>
                <option>Software Developing</option>
                <option>IT & Networking</option>
                <option>Writing</option>
                <option>Translation</option>
                <option>Sales & Marketing</option>
            </select>
        </div>

        <!-- Tags -->
        <div class="sidebar-widget">
            <h3>Compétences</h3>

            <div class="tags-container">
                <div class="tag">
                    <input type="checkbox" id="tag1"/>
                    <label for="tag1">front-end dev</label>
                </div>
                <div class="tag">
                    <input type="checkbox" id="tag2"/>
                    <label for="tag2">angular</label>
                </div>
                <div class="tag">
                    <input type="checkbox" id="tag3"/>
                    <label for="tag3">react</label>
                </div>
                <div class="tag">
                    <input type="checkbox" id="tag4"/>
                    <label for="tag4">vue js</label>
                </div>
                <div class="tag">
                    <input type="checkbox" id="tag5"/>
                    <label for="tag5">web apps</label>
                </div>
                <div class="tag">
                    <input type="checkbox" id="tag6"/>
                    <label for="tag6">design</label>
                </div>
                <div class="tag">
                    <input type="checkbox" id="tag7"/>
                    <label for="tag7">wordpress</label>
                </div>
            </div>
            <div class="clearfix"></div>


        </div>
        <div class="clearfix"></div>

    </div>
</div>
