@extends('welcome')


@section('css')

@endsection


@section('content')
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Devenir Expert Certifier</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Accueil</a></li>
						<li><a href="#">Pages</a></li>
						<li>Freelancer</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>
<div class="container">
    <div class="section padding-top-60 padding-bottom-75">
        <div class="container">
            <div id="contenu">
                <div class="sous-contenu1">
                    <img src="images/banner/pexels-canva-studio-3194519.jpg" style="border-radius: 20px" class="img-ab" width="1200em" height="420em" alt="">
                </div>
                <div  class="sous-contenu2 ml-4">
                    <div class="col-12 col-sm-12">
                        <p> <strong style="color: #2a41e8; font-size: 2em">Les ateliers Pratiques</strong></p>
                    </div>
                    <div class="col-12 col-sm-12" >
                    <p class="text-justify">
                        Le lorem ipsum est, en imprimerie,
                        une suite de mots sans signification
                        utilisée à titre provisoire pour calibrer
                        une mise en page, le texte définitif venant
                        remplacer le faux-texte dès qu'il est prêt
                        ou que la mise en page est achevée.
                        Généralement, on utilise un texte en faux
                        latin, le Lorem ipsum ou Lipsum.

                    </p>
                    </div>
                    <div class="col-6 col-sm-12 mt-3">
                        <a href="https://www.lesatelierspratiques.com" class="button" target="_blank">Visitez le site des ateliers pratiques</a>
                    </div>
                    <div class="col-6 col-sm-12">
                        <img src="{{ asset('images/images/logo Ateliers Pratiques-01.png') }}" width="200px"  alt="">
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-6">
                </div>
                <div class="col-6">
                    <div class="col-6 col-sm-12">
                        <p> <strong style="color: #2a41e8; font-size: 2em">Les ateliers Pratiques</strong></p>
                    </div>
                    <div class="col-6 col-sm-12" >
                    <p class="text-justify">
                        Le lorem ipsum est, en imprimerie,
                        une suite de mots sans signification
                        utilisée à titre provisoire pour calibrer
                        une mise en page, le texte définitif venant
                        remplacer le faux-texte dès qu'il est prêt
                        ou que la mise en page est achevée.
                        Généralement, on utilise un texte en faux
                        latin, le Lorem ipsum ou Lipsum.

                    </p>
                    </div>
                    <div class="col-6 col-sm-12 mt-3">
                        <a href="https://www.lesatelierspratiques.com" class="button" target="_blank">Visitez le site des ateliers pratiques</a>
                    </div>
                    <div class="col-6 col-sm-12">
                        <img src="{{ asset('images/images/logo Ateliers Pratiques-01.png') }}" width="200px"  alt="">
                    </div>
                </div>
            </div> --}}
        </div>

    </div>

</div>
@endsection

@section('script')

@endsection
