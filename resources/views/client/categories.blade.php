@extends('welcome')


@section('css')
{{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> --}}
@endsection


@section('content')
    <div class="full-page-container">

        @include('client.includes.categorie.sidebar')
        @include('client.includes.categorie.content')


    </div>

@endsection


@section('script')

    <script>
            $(document).ready(function() {
                //afficher les sous catégorie


                $('#titre').on('change', function() {
                    if ($("#titre").val() != "") {
                        $.ajax({
                            url: '/client/chercher/souscompetence/' + $("#titre").val(),
                            type: 'GET',
                            success: function(response) {
                                $("#sousCat").html('');
                                if (response.array) {
                                    response.array.forEach(element => {
                                        $("#sousCat").append('<option value="' + element
                                            .titre_sous_categorie + '">' + element
                                            .titre_sous_categorie + ' </option>')
                                    });
                                }
                            },
                            error: function(xhr) {
                                alert("Impossible de récupérer ces données");
                            }
                        });
                    }

                });
            })
        // Snackbar for user status switcher
        $('#snackbar-user-status label').click(function() {
            Snackbar.show({
                text: 'Your status has been changed!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#383838'
            });
        });
    </script>

    <!-- Google Autocomplete -->
    <script>
        function initAutocomplete() {
            var options = {
                types: ['(cities)'],
                // componentRestrictions: {country: "us"}
            };

            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input, options);
        }
    </script>

    <!-- Google API & Maps -->
    <!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->
    <script src="https://maps.googleapis.com/maps/api/js?key=&libraries=places"></script>

@endsection
