@extends('welcome')


@section('css')

@endsection


@section('content')
    <div class="full-page-container">

        @include('client.includes.categorie.sidebar')
        @include('client.includes.categorie.content')

    </div>
@endsection

@section('script')

@endsection
