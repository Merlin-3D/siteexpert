@extends('welcome')


@section('css')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
@endsection


@section('content')

    <div id="titlebar" class="gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Contact</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Accueil</a></li>
                            <li><a href="#">Pages</a></li>
                            <li>Contact</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>


    <!-- Content
        ================================================== -->


    <!-- Container -->
    <div class="container">
        <div class="row">

            <div class="col-xl-12">
                <div class="contact-location-info margin-bottom-50">
                    <div class="contact-address">
                        <ul>
                            <li class="contact-address-headline">Notre bureau</li>
                            <li>425 Yaoundé Polytechnique, CM 93584</li>
                            <li>Télèphone (+237)655332183</li>
                            <li><a href="#">contact@afrik-solutions.com</a></li>
                            <li>
                                <div class="freelancer-socials">
                                    <ul class="footer-social-links">
                                        <li>
                                            <button class="button" type="submit"><i class="icon-brand-linkedin-in"></i>
                                            </button>

                                        </li>
                                        <li>
                                            <button class="button ml-2 mr-2" type="submit"><i
                                                    class="icon-brand-facebook-f  pt-5"></i>
                                            </button>
                                        </li>
                                        <li>
                                            <button class="button" type="submit"><i class="icon-brand-twitter"></i>
                                            </button>

                                        </li>

                                    </ul>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div id="single-job-map-container">
                        <div id="singleListingMap" data-latitude="37.777842" data-longitude="-122.391805"
                            data-map-icon="im im-icon-Hamburger"></div>
                        <a href="#" id="streetView">Vue sur la rue</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-8 col-lg-8 offset-xl-2 offset-lg-2">

                <section id="contact" class="margin-bottom-60">
                    <h3 class="headline margin-top-15 margin-bottom-35">Des questions? N'hésitez pas à nous contacter!</h3>
                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    <form method="post" action="{{ route('client.envoyer_contact') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-with-icon-left">
                                    <input class="with-border" name="name" type="text" id="name" placeholder="Votre nom" />
                                    <i class="icon-material-outline-account-circle"></i>
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger">
                                            {{ $errors->first('name') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="input-with-icon-left">
                                    <input class="with-border" name="email" type="email" id="email"
                                        placeholder="Adresse e-mail"
                                        pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" />
                                    <i class="icon-material-outline-email"></i>
                                    @if ($errors->has('email'))
                                        <div class="alert alert-danger">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="input-with-icon-left">
                            <input class="with-border" name="phone" type="text" id="phone" placeholder="Télèphone" />
                            <i class="icon-feather-phone"></i>
                            @if ($errors->has('phone'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('phone') }}
                                </div>
                            @endif
                        </div>
                        <div class="input-with-icon-left">
                            <input class="with-border" name="subject" type="text" id="subject" placeholder="Sujet" />
                            <i class="icon-material-outline-assignment"></i>
                            @if ($errors->has('subject'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('subject') }}
                                </div>
                            @endif
                        </div>

                        <div>
                            <textarea class="with-border" name="message" cols="40" rows="5" id="comments"
                                placeholder="Message" spellcheck="true"></textarea>
                            @if ($errors->has('message'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('message') }}
                                </div>
                            @endif
                        </div>

                        <input type="submit" class="submit button margin-top-15" id="submit" value="Soumettre un message" />

                    </form>
                </section>

            </div>

        </div>
    </div>

@endsection


@section('script')
    <script>
        $('#snackbar-user-status label').click(function() {
            Snackbar.show({
                text: 'Your status has been changed!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#383838'
            });
        });
    </script>

    <script src="{{ asset('js/infobox.min.js') }}"></script>
    <script src="{{ asset('js/markerclusterer.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=&libraries=places"></script>

    <script src="{{ asset('js/maps.js') }}"></script>

@endsection
