@extends('welcome')


@section('css')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

@endsection


@section('content')
@include('client.includes.accueil.banner')

@if(Session::has('error'))
<script type="text/javascript" >
    alert("{{ session()->get('error') }}");

</script>

@endif


@include('client.includes.accueil.list_prestataire')
@include('client.includes.accueil.most_visite')

@include('client.includes.accueil.popular_categorys')
@include('client.includes.accueil.about_us')
@include('client.includes.accueil.testimonial')
@include('client.includes.accueil.partner')
@endsection


@section('script')
<script>
    function rechercheAccueil() {
                var ville = $('#ville').val()
                var service = $('#service').val()
                var id_categorie = $('#id_categorie').val()

                $.ajax({
                    url: "{{ route('client.rechercher_accueil') }}",
                    type: "POST",
                    contentType: 'application/x-www-form-urlencoded',
                    data: {
                        "_token": '{{ csrf_token() }}',
                        ville: ville,
                        service: service,
                        id_categorie: id_categorie
                    },
                    dataType: 'JSON',
                    success: function(response) {
                        if (response) { // when status code is 422, it's a validation issue

                        console.log(response)
                        }
                    },
                    error: function(err) {
                       console.log(err)
                    }
                });
            }
</script>
@endsection
