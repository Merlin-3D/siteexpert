@extends('welcome')


@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <style>
        .fb-profile-block {
            margin: auto;
            position: relative;
            width: 100%;
        }

        .cover-container {
            background-image: url("{{ $prestataires[0]->banniere ? asset('/bannieres/' . $prestataires[0]->banniere) : asset('images/banner/pexels-canva-studio-3194519.jpg') }}");     }

        .fb-profile-block-thumb {
            display: block;
            height: 315px;
            overflow: hidden;
            position: relative;
            text-decoration: none;
        }

        .fb-profile-block-menu {
            border: 1px solid #d3d6db;
            border-radius: 0 0 3px 3px;
        }

        .profile-img a {
            bottom: 15px;
            box-shadow: none;
            display: block;
            left: 15px;
            padding: 1px;
            position: absolute;

            height: 160px;
            width: 160px;
            z-index: 9;
        }

        .profile-img img {
            background-color: #fff;
            border-radius: 2px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.07);
            height: 158px;
            padding: 5px;
            width: 158px;
        }

        .profile-name {
            bottom: 60px;
            left: 200px;
            position: absolute;
        }

        .profile-name h2 {
            color: #fff;
            font-size: 24px;
            font-weight: 600;
            line-height: 30px;
            max-width: 275px;
            position: relative;
            text-transform: uppercase;
        }

        .fb-profile-block-menu {
            height: 44px;
            position: relative;
            width: 100%;
            overflow: hidden;
        }

        .block-menu {
            clear: right;
            padding-left: 205px;
        }

        .block-menu ul {
            margin: 0;
            padding: 0;
        }

        .block-menu ul li {
            display: inline-block;
        }

        .block-menu ul li a {
            border-right: 1px solid #e9eaed;
            float: left;
            font-size: 14px;
            font-weight: bold;
            height: 42px;
            line-height: 3.0;
            padding: 0 17px;
            position: relative;
            vertical-align: middle;
            white-space: nowrap;
            color: #4b4f56;
            text-transform: capitalize;
        }

        .block-menu ul li:first-child a {
            border-left: 1px solid #e9eaed;
        }

        #myImg {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
        }

        #myImg:hover {
            opacity: 0.7;
        }

        /* The Modal (background) */
        .modal {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Stay in place */
            z-index: 1;
            /* Sit on top */
            padding-top: 100px;
            /* Location of the box */
            left: 0;
            top: 0;
            width: 100%;
            /* Full width */
            height: 100%;
            /* Full height */
            overflow: auto;
            /* Enable scroll if needed */
            background-color: rgb(0, 0, 0);
            /* Fallback color */
            background-color: rgba(0, 0, 0, 0.9);
            /* Black w/ opacity */
        }

        /* Modal Content (image) */
        .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }

        /* Caption of Modal Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }

        /* Add Animation */
        .modal-content,
        #caption {
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
            from {
                -webkit-transform: scale(0)
            }

            to {
                -webkit-transform: scale(1)
            }
        }

        @keyframes zoom {
            from {
                transform: scale(0)
            }

            to {
                transform: scale(1)
            }
        }

        /* The Close Button */
        .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px) {
            .modal-content {
                width: 100%;
            }
        }

    </style>




@endsection


@section('content')

    @include('client.includes.prestataire.header')

    <div class="container">
        <div class="row">
            @include('client.includes.prestataire.content')
            @include('client.includes.prestataire.sidebar')
        </div>
        <div id="myModal" class="modal">
            <span class="close">×</span>
            <img class="modal-content" id="img01">
            <div id="caption"></div>
        </div>


    </div>

@endsection


@section('script')
    <script>

        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('myImg');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function() {
            modal.style.display = "block";
            modalImg.src = this.src;
            modalImg.alt = this.alt;
            captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

    </script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#add-comment').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $('#commentaire-error').text('');

            $.ajax({
                type: 'POST',
                url: "{{ route('client.ajouter_commentaire') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response) {
                        console.log(response)
                        // notification.innerHTML =
                        //     `<div class="notification success closeable mt-3">
                    //     <p> ${response.message} </p>
                    //     <a class="close" href="#"></a>
                    // </div>`

                        $('.liste-comment').append(`
                                <li id="comment_${ response.array.id }">
                                <div class="boxed-list-item">
                                    <!-- Content -->
                                    <div class="item-content">
                                        <h4> <i>Par vous</i>
                                        </h4>
                                        <div class="item-details margin-top-10">
                                            <div class="detail-item"><i class="icon-material-outline-date-range"></i>
                                               <i> Maintenant</i> </div>
                                        </div>
                                        <div class="item-description">
                                            <p class="text-justify">${ response.array.commentaire } </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        `)

                        this.reset();
                    }
                },
                error: function(response) {
                    console.log(response.responseJSON)
                    $('#commentaire-error').text(response.responseJSON.errors.commentaire);
                }
            });

        });

        function count_click(id) {
            url = "/client/count/clickContact/" + id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "GET",
                url: url,
                success: function(response) {

                }
            });
        }
    </script>

@endsection
