<div id="footer">

	<!-- Footer Top Section -->
	<div class="footer-top-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">

					<!-- Footer Rows Container -->
					<div class="footer-rows-container">

						<!-- Left Side -->
						<div class="footer-rows-left">
							<div class="footer-row">
								<div class="footer-row-inner footer-logo">
                                    <a href="{{ route('/') }}"><img src="{{ asset('images/logo/les-experts-blanc.png') }}" alt=""></a>

								</div>
							</div>
						</div>

						<!-- Right Side -->
						<div class="footer-rows-right">

							<!-- Social Icons -->
							<div class="footer-row">
								<div class="footer-row-inner">
									<ul class="footer-social-links">
                                        <li>
                                            <button class="button" type="submit"><i class="icon-brand-linkedin-in"></i>
                                            </button>

										</li>
										<li>
                                            <button class="button ml-2 mr-2" type="submit"><i class="icon-brand-facebook-f  pt-5"></i>
                                            </button>
										</li>
										<li>
                                            <button class="button" type="submit"><i class="icon-brand-twitter"></i>
                                            </button>

										</li>

									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>

					</div>
					<!-- Footer Rows Container / End -->
				</div>
			</div>
		</div>
	</div>
	<!-- Footer Top Section / End -->

	<!-- Footer Middle Section -->
	<div class="footer-middle-section">
		<div class="container">
			<div class="row">

				<!-- Links -->
				<div class="col-xl-4 col-lg-2 col-md-3">
					<div class="footer-links">
						<h3>FA propos</h3>
						<ul>
							<li><a href="#"><span>Les meilleurs freelancers de la région, qui
                                mettent leur compétence en avant pour
                                vous produire des services de qualité. Alors
                                n’attendez plus. Trouvez votre profil</span></a></li>

							<li>
                                <button class="button mt-3">Trouver le votre</button>
                            </li>
						</ul>
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3 ml-5">
					<div class="footer-links">
						<h3>Catégories</h3>
						<ul>
							<li><a href="#"><span>Developpement Web</span></a></li>
							<li><a href="#"><span>Design Graphique</span></a></li>
							<li><a href="#"><span>Architecture</span></a></li>
							<li><a href="#"><span>Assistant de direction</span></a></li>
						</ul>
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3 ml-5">
					<div class="footer-links">
						<h3>Accès Rapide</h3>
						<ul>
							<li><a href="#"><span>Accueil</span></a></li>
							<li><a href="#"><span>Les experts</span></a></li>
							<li><a href="#"><span>Contact</span></a></li>
							<li><a href="#"><span>Les ateliers pratiques</span></a></li>
						</ul>
					</div>
				</div>

				<!-- Links -->
				<div class="col-xl-2 col-lg-2 col-md-3 ml-5">
					<div class="footer-links">
						<h3>Partenaires</h3>
						<ul>
							<li><a href="#"><span>Les Ateliers Pratiques</span></a></li>
							<li><a href="#"><span>Afrik Solutions</span></a></li>
                            <li><a href="#"><span>Heka Design</span></a></li>
                            <li><a href="#"><span>Technipole</span></a></li>
						</ul>
					</div>
				</div>


			</div>
		</div>
	</div>
	<!-- Footer Middle Section / End -->

	<!-- Footer Copyrights -->
	<div class="footer-bottom-section">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<strong>© 2020 Les Experts</strong> All Rights Reserved. Powered by <strong>Afrik Solutions</strong>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer Copyrights / End -->

</div>
