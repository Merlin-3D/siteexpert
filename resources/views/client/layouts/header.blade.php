<header id="header-container" class="fullwidth transparent">

    <!-- Header -->
    <div id="header">
        <div class="container">

            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Logo -->
                <div id="logo">
                    <a href="{{ route('/') }}"><img src="{{ asset('images/logo/les-experts-bleu.png') }}"
                            alt=""></a>
                </div>

                <!-- Main Navigation -->
                <nav id="navigation">
                    <ul id="responsive" class="">

                        <li><a href="{{ route('/') }}" style="margin-top: 3px">Accueil</a>

                        </li>

                        <li><a href="#">Catégories</a>
                            <ul class="dropdown-nav">
                                <li><a href="{{ route('page_categorie', '0') }}">Tout</a></li>
                                @foreach ($categories as $categorie)

                                    @if (count($categorie->souscategorie) > 0)
                                        <li><a href="#">{{ $categorie->titre }}</a>
                                            <ul class="dropdown-nav">
                                                @foreach ($categorie->souscategorie as $souscategorie)
                                                    <li><a
                                                            href="{{ route('page_categorie', $categorie->id) }}">{{ $souscategorie->titre_sous_categorie }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else

                                        <li><a
                                                href="{{ route('page_categorie', $categorie->id) }}">{{ $categorie->titre }}</a>
                                        </li>

                                    @endif
                                @endforeach

                            </ul>
                        </li>

                        <li><a href="#">Ville</a>
                            <ul class="dropdown-nav">

                                <li><a href="{{ route('page_ville', 'yaounde') }}">Yaoundé</a></li>
                                <li><a href="{{ route('page_ville', 'douala') }}">Douala</a></li>
                                <li><a href="{{ route('page_ville', 'Maroua') }}">Maroua</a></li>
                            </ul>
                        </li>

                        <li><a href="{{ route('page_contact') }}" style="margin-top: 3px">Contact</a>

                        </li>

                        <li><a href="{{ route('page_devenir') }}" style="margin-top: 3px">Devenir Expert Certifier</a>

                        </li>

                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->

            </div>
            <!-- Left Side Content / End -->


            <!-- Right Side Content / End -->
            <div class="right-side">

                <div class="header-widget">

                    <!-- Messages -->
                    <div class="header-notifications user-menu">

                        @if (Route::has('login'))
                            <!-- Dropdown -->

                            @auth
                                <div class="header-notifications-trigger">
                                    <a href="#">
                                        <div class="user-avatar status-online"><img src="{{asset('images/company-logo-05.png') }}"
                                                alt=""></div>
                                    </a>
                                </div>
                                <div class="header-notifications-dropdown">

                                    <!-- User Status -->
                                    <div class="user-status">

                                        <!-- User Name / Avatar -->
                                        <div class="user-details">
                                            <div class="user-avatar status-online"><img
                                                src="{{ asset('images/company-logo-05.png') }}" alt=""></div>
                                            <div class="user-name">
                                                {{ Auth::user()->name }}
                                            </div>
                                        </div>

                                    </div>

                                    <ul class="user-menu-small-nav">
                                        @if (auth()->user()->hasRole('PRESTATAIRE'))

                                            <li><a href="{{ route('prestataires.accueil',Auth::user()->id) }}"><i
                                                        class="icon-material-outline-dashboard"></i>
                                                    Dashboard</a></li>
                                        @elseif (auth()->user()->hasRole('CLIENT'))
                                            <li><i class="icon-feather-user"></i>
                                                Client</li>

                                        @endif

                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                                                <i class="icon-material-outline-power-settings-new"></i>
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                class="d-none">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>

                                </div>
                            @else

                                <a href="#sign-in-dialog" style="margin-top: 15px" class="button popup-with-zoom-anim mt-3">
                                    <span><i class="icon-feather-user"></i> Mon
                                        Compte</span></a>

                            @endauth

                        @endif
                    </div>

                </div>
                <!-- Mobile Navigation Button -->
                <span class="mmenu-trigger">
                    <button class="hamburger hamburger--collapse" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </span>

            </div>
            <!-- Right Side Content / End -->

        </div>
    </div>
    <!-- Header / End -->

</header>
<div class="clearfix"></div>
