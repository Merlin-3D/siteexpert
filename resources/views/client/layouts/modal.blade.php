<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#login">Se connecter</a></li>
            <li><a href="#register">S'inscrire</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Login -->
            <div class="popup-tab-content" id="login">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Nous sommes heureux de vous revoir!</h3>
                    <span>Vous n'avez pas de compte ? <a href="#" class="register-tab">S'inscrire!</a></span>
                </div>

                <!-- Form -->
                <form method="POST" action="{{ route('login') }}" id="login-form">
                    @csrf
                    <div class="input-with-icon-left">
                        <i class="icon-material-baseline-mail-outline"></i>
                        <input id="email" type="email"
                            class="input-text with-border @error('email') is-invalid @enderror" name="email"
                            value="{{ old('email') }}" required autocomplete="email" placeholder="Adresse e-mail" />
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input id="password" type="password"
                            class="input-text with-border @error('password') is-invalid @enderror" name="password"
                            required autocomplete="current-passwordname" placeholder="Mot de passe" required
                            autocomplete="current-password" />
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}" class="forgot-password">Mot de passe oublié?</a>
                    @endif
                    <button class="button full-width button-sliding-icon ripple-effect" type="submit"
                        form="login-form">Connexion <i class="icon-material-outline-arrow-right-alt"></i></button>

                </form>

                <!-- Button -->

            </div>

            <!-- Register -->
            <div class="popup-tab-content" id="register">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Créons votre compte !</h3>
                    <span>Vous possédez déja un compte ? <a href="#" class="login-tab">Se connecter !</a></span>
                </div>
                <!-- Form -->
                <form method="POST" action="{{ route('creer_client') }}" id="register-account-form">
                    @csrf
                    <div class="input-with-icon-left">
                        <i class="icon-feather-user"></i>
                        <input type="text" class="input-text with-border @error('name') is-invalid @enderror" placeholder="Votre Nom" name="name" value="{{ old('name') }}" required autocomplete="name" />
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

                    <div class="input-with-icon-left">
                        <i class="icon-material-baseline-mail-outline"></i>
                        <input type="email" class="input-text with-border @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Adresse e-mail" required autocomplete="email"/>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>

                    <div class="input-with-icon-left" title="Should be at least 8 characters long"
                        data-tippy-placement="bottom">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border @error('password') is-invalid @enderror" name="password"  placeholder="Mot de passe"required autocomplete="new-password"/>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    {{-- <div class="input-with-icon-left">
                        <i class="icon-material-outline-lock"></i>
                        <input type="password" class="input-text with-border"
                        id="password-confirm" name="password_confirmation" required autocomplete="new-password" />
                    </div> --}}
                    <!-- Button -->
                    <button class="margin-top-10 button full-width button-sliding-icon ripple-effect" type="submit"
                        form="register-account-form">S'inscrire <i
                            class="icon-material-outline-arrow-right-alt"></i></button>
                </form>


            </div>

        </div>
    </div>
</div>
